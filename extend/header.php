<?php
    include '../Conexion/conexion.php';
    //evita que se reabra la sesion una vez cerrado la pagina
    if (!isset($_SESSION['nick'])) {
      header('location: ../');
      //cuando se ingresa del login se general las variables de sesion desde el login, aqui si existe se redirecciona a inicio
    }


     ?>
<!DOCTYPE html> <html lang="es"> <head> <meta charset="utf-8"> <meta name="viewport" content="width=device-width , initial-scale=1.0"> <meta http-equiv="X-UA-Compatible" content="ie-edge"> <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> <meta http-equiv="refresh" content="1800"/> <link rel="stylesheet" href="../css/materialize.min.css"> <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><!--<style type="text/css"> .button{background-color: #4CAF50; /* Green */ border-radius: 12px; color: white; padding: 5px; text-align: center; text-decoration: none; display: inline-block; font-size: 12px;}.button:hover{background-color: green; color:white;}</style>--> <div class="loader"></div><style>.loader{position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 9999; background: url('../img/5.gif') 50% 50% no-repeat rgb(249,249,249); opacity: .8;}</style> <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> <script type="text/javascript">$(window).load(function(){$(".loader").fadeOut("slow");}); </script> <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> <style>.card{width: 1500px; padding: 20px;}</style> <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.css"> <style media="screen"> header, main, footer{padding-left: 300px;}.button-collpase{display: none;}@media only screen and (max-width : 100%){header, main, footer{padding-left: 0;}.button-collpase{display: inherit;}}</style> <style>@media only screen and (max-width : 100%){.card{width:100%;}}.card{width:1500px;}</style><script LANGUAGE="JavaScript"></script> <title>Proyecto</title> </head> <body class="grey lighten-3"> <div class="loader"></div><style>.loader{position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 9999; background: url('../img/5.gif') 50% 50% no-repeat rgb(249,249,249); opacity: .8;}</style> <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> <script type="text/javascript">$(window).load(function(){$(".loader").fadeOut("slow");}); </script> <main>
<meta content="Autor" name="Ing Luis Fernando Jonathan Vargas Osornio">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <link rel="stylesheet" href="http://jschr.github.io/bootstrap-modal/css/bootstrap-modal-bs3patch.css" />
    <link rel="stylesheet" href="http://jschr.github.io/bootstrap-modal/css/bootstrap-modal.css" />
    <link rel="stylesheet" href="css/materialize.min.css"> <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="http://jschr.github.io/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="http://jschr.github.io/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.css">
    <link rel="icon" href="../img/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="apple-touch-icon-precomposed" href="../img/favicon/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="../img/favicon/mstile-144x144.png">
    <style type="text/css">
        body{

        }
        input:-ms-input-placeholder {
            font-size: 20px;
            color: black;
        }

    </style>
<style>

::-webkit-input-placeholder { /* WebKit, Blink, Edge */
color: black !important;/*!Important sirve para dar orden inmediata de cambiar el color*/
opacity: 1;
}
:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
color: black !important;
/* esto es porque Firefox le reduce la opacidad por defecto */;
opacity: 1;
}
::-moz-placeholder { /* Mozilla Firefox 19+ */
color: black !important;
opacity: 1;

}
:-ms-input-placeholder { /* Internet Explorer 10-11 */
color: black !important;
}</style>

      <?php
      //Discrimina para asignar los accesos al login

      //AGREGA LOS IFS PARA LOS MODULOS menu_MODULO!.php
      if ($_SESSION['nivel'] == 'ADMINISTRADOR') {
        include '../extend/menu_admin.php';
      } elseif ($_SESSION['nivel'] == 'MESADECONTROL') {
        include '../extend/menu_asesor.php';
      }elseif ($_SESSION['nivel'] == 'COMPRAS') {
        include '../extend/menu_compras.php';
      }elseif ($_SESSION['nivel'] == 'CUENTASPORPAGAR') {
        include '../extend/menu_cuentasporpagar.php';
      }elseif ($_SESSION['nivel'] == 'CONTABILIDAD') {
        include '../extend/menu_contabilidad.php';
      }elseif ($_SESSION['nivel'] == 'HISTORIAL') {
        include '../extend/menu_historial.php';
      }elseif ($_SESSION['nivel'] == 'GERENCIA') {
        include '../extend/menu_gerente.php';
      }
      else{
        echo "Ingreso incorrecto";
      }
      //error_reporting(0);
        ?>

    </body>
    </html>
