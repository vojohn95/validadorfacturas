<?php include '../extend/header.php';

//include '../Conexion/conexion.php';
include '../conexionEBS/conexion.php';

?>

<!--Buscador en la tabla-->
<div class="row">
  <div class="col s12" style ="width: 1330px;">
    <nav class="green lighten-1" >
      <div class="nav-wrapper" >
        <div class="input-field">
          <input type="search" id="buscar" autocomplete="off">
          <label for="buscar"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div>
      </div>
    </nav>
  </div>
</div>
<!-- termina buscador-->
<?php
$rfcEbs = $_POST['Videbs'];
//echo $rfcEbs;
/*
error_reporting(0);
/*
 *	$Id: wsdlclient1.php,v 1.3 2007/11/06 14:48:48 snichol Exp $
 *
 *	WSDL client sample.
 *
 *	Service: WSDL
 *	Payload: document/literal
 *	Transport: http
 *	Authentication: none
 */
/*
require_once('ins_archivo/lib/nusoap.php');
$proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
$proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
$proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
$client = new nusoap_client('http://localhost/pruebadfacture/WsdlEbs/webservice.php?wsdl&debug=1', 'wsdl',
						$proxyhost, $proxyport, $proxyusername, $proxypassword);
$err = $client->getError();
if ($err) {
	echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
}
// Doc/lit parameters get wrapped
$param = array(
	'user'    => 'DEMOVcueto',
	'password' => 'cfdi',
	'rfcReceptor' => 'SCE940913BX0'
);

$client->soap_defencoding = 'UTF-8';
$client->decode_utf8 = true;

//$result = $client->call('ConsultarValidacionesCFDI', array('parameters' => $param), '', '', false, true);
$result = $client->call('ConsultaVid', array('parameters' => "$rfcEbs"), '', '', false, true);

// Check for a fault
if ($client->fault) {
	//echo '<h2>Fault</h2><pre>';
	//print_r($result);
	//echo '</pre>';
} else {
	// Check for errors
	$err = $client->getError();
	if ($err) {
		// Display the error
		//echo '<h2>Error</h2><pre>' . $err . '</pre>';
	} else {
		// Display the result
		//echo '<h2>Resultado</h2><pre>';
		//print_r($result);
		//echo '</pre>';
	}
}
 '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
 '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
 '<h2>Debug</h2><pre>' . htmlspecialchars($client->debug_str, ENT_QUOTES) . '</pre>';

 foreach ($result as $key => $value) {
   $mensaje = $value['NUMERO_DE_PROVEEDOR'];
   $noCert = $value['RAZON_SOCIAL'];
   $noCont = $value['RFC'];
 }
 if($mensaje == NULL && $noCert == NULL && $noCont == NULL){

?>
<!--Sweetlaert2 que redirige cuando trae valores nulos-->
<?php $dir = 'proveedorcomp.php';?>
<script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>
<script>
  swal({
    title: '<?php echo "Sin registros"; ?>',
    text: '<?php echo "No se encontro en ebs"; ?>',
    type: 'error',
    confirmButtonColor: '#3085d6',
    confirmButtonText: 'ok'
  }).then(function(){
    location.href='<?php echo $dir ?>';
  });

  $(document).click(function(){
    location.href='<?php echo $dir ?>';
  });

  $(document).keyup(function(e){
    if (e.which == 27) {
      location.href='<?php echo $dir ?>';
    }
  });

</script>

<?php }else{ ?>

<?php
/*Consulta a ebs anterior
$valor_rfc = 'SELECT  distinct(aps.segment1) "NUMERO PROVEEDOR",
       aps.vendor_name "PROVEEDOR",
       APS.NUM_1099 "RFC",
       aps.creation_date "FECHA ALTA",
       fu.email_address "E-MAIL",
       fu.user_name "CREADO POR"
       FROM ap.ap_suppliers aps,
       applsys.fnd_user fu
       WHERE aps.created_by = fu.user_id
       AND APS.NUM_1099 = ' . $rfcEbs;
       $stid =  oci_parse($conn, $valor_rfc);
       echo $valor_rfc;*/

$stid = oci_parse($conn, "SELECT * FROM AP_SUPPLIERS WHERE VENDOR_ID = '$rfcEbs'");
oci_execute($stid);
while (oci_fetch($stid)) {
    $numProv = oci_result($stid, 'VENDOR_ID') ;
    $Prov = oci_result($stid, 'VENDOR_NAME') ;
    $fec = oci_result($stid, 'NUM_1099') ;
    $ulti = oci_result($stid, 'LAST_UPDATE_DATE') ;
}

// Muestra:
//   1000 es Roma
//   1100 es Venice

oci_free_statement($stid);
oci_close($conn);
//var_dump(isset($numProv));
if (isset($numProv) == false){ ?>
    <script>
        alert("Sin registros en ebs");
        location.href ="proveedorcomp";
    </script>

    <?php
}

?>

<div class = 'row'>
  <div class = 'col s12'>
    <div class = 'card'>
      <div class = "card-content">
        <span class = 'card-title'>Resultados:</span>
        <table table border='1' class="centered">
          <thead>
            <tr class="cabecera">
              <!--<th>Ticket</th>-->
                <th>No.Proveedor</th>
                <th>Proveedor</th>
                <th>RFC</th>
                <th>Ultima actualizacion</th>
                <th>     </th>
                <!--<th>Creado por</th>-->
                <th></th>

            </tr>
          </thead>
            <tr>
                <td> <?php echo $numProv; ?></td>
                <td> <?php echo $Prov; ?></td>
                <td> <?php echo $fec; ?></td>
                <td> <?php echo $ulti; ?></td>

            </tr>


<?php
          /*
          //Consulta general a ebs
        while ($row =@ oci_fetch_row($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
            echo "<tr>";
            foreach ($row as $item) {
                echo "<td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "") . "</td>";
            }
            echo "</tr>";
        }
        echo "</table>\n";
        /*while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {?>
          <tr>
            <td><?php echo $row['NUMERO PROVEEDOR'] ?></td>
            <td><?php echo $row['PROVEEDOR'] ?></td>
            <td><?php echo $row['RFC'] ?></td>
            <td><?php echo $row['FECHA ALTA'] ?></td>
            <td><?php echo $row['CREADO POR'] ?></td>
            </tr>
          </table>
          </tr>
        <?php } ?>
      </table>
*/
          ?>
      </div>
    </div>
  </div>
</div>
<div>
 <a href="proveedorcomp"><i class="material-icons">keyboard_return</i>REGRESO</a>
</div>

</body>

<?php //} ?>

 <?php include '../extend/scripts.php'; ?>
 <script src="../js/validacion.js"></script>

 </html>
