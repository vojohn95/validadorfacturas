<?php include '../extend/header.php';
  //Restringe a usuarios para no entrar a paginas no permitidas
include '../extend/permiso.php';
//include 'ins_archivo/ins_archivos.php';
//include '../Conexion/conexion.php';

 ?>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
 <!--Buscador en la tabla-->
 <div class="row">
   <div class="col s12" style ="width: 1500px;">
     <nav class="green lighten-1" >
       <div class="nav-wrapper" >
         <div class="input-field">
           <input type="search" id="buscar" autocomplete="off">
           <label for="buscar"><i class="material-icons">search</i></label>
           <i class="material-icons">close</i>
         </div>
       </div>
     </nav>
   </div>
 </div>
 <!-- termina buscador-->


 <?php

 //consulta 5.4
 //$sel = "SELECT ticket,proveedor, rfc , fecha , importe_iva , num_factura , uuid, comentario, estatus FROM ticket WHERE estatus = 'CUENTASPORPAGAR' ";
 //$consulta = mysql_query($sel,$localhost);
 //  $var = mysql_fetch_assoc($consulta) or die ('no se pudo hacer la consulta'.mysql_error());
 //$row = mysql_num_rows($consulta);
 //Version 7.3
 $query= "SELECT ticket,proveedor, rfc , fecha , importe_iva , num_factura , uuid, comentario, estatus FROM ticket WHERE estatus = 'CUENTASPORPAGAR'";
 $consulta = mysqli_query($mysqli, $query) or die('Error al buscar en la base de datos.');
 $row = mysqli_num_rows($consulta);
  ?>

 <div class="row">
   <div class="col s12" style ="width: 1500px;">
     <div class="card hoverable">
       <div class="card-content">
         <span class="card-title">Archivos pendientes(<?php echo $row ?>)</span>
         <table  class="centered">
           <thead>
             <tr class="cabecera">
               <th>Visualizar</th>
               <th>Ticket</th>
               <th>Proveedor</th>
                 <th>Rfc</th>
               <th>Fecha</th>
               <th>Importe_IVA</th>
               <th>UUID</th>
               <th>Comen.anterior</th>
               <th>Comentario</th>
                 <th>Pdf</th>
                 <th>XML</th>
                 <th>Visualizar cuentas</th>
               <th>Rechazado</th>
               <th>Valido</th>
               <th>Descarga archivos</th>
             </tr>
           </thead>

           <?php if($row >= 1){ ?>
           <script>
            Notification.requestPermission();

            function spawnNotification(theBody,theIcon,theTitle) {
              var options = {
                  body: theBody,
                  icon: theIcon
              }
              var n = new Notification(theTitle,options);
              setTimeout(n.close.bind(n), 5000);
            }

            spawnNotification("Tienes facturas por validar pendientes!!", undefined, "Estatus");
           </script>
         <?php }else{ ?>
           <script>
            Notification.requestPermission();

            function spawnNotification(theBody,theIcon,theTitle) {
              var options = {
                  body: theBody,
                  icon: theIcon
              }
              var n = new Notification(theTitle,options);
              setTimeout(n.close.bind(n), 5000);
            }

            spawnNotification("Sin facturas pendientes!!", undefined, "Estatus");
           </script>
       <?php } ?>

           <?php

           while ($f=mysqli_fetch_assoc($consulta)) { ?>
             <tr><!--Modal-->
               <td><a class="btn-floating btn-medium waves-effect waves-lighten blue" onclick="swal({ title: 'Visualizar informacion de factura', type: 'info', showCancelButton: true, confirmButtonColor:
               '#3085d6', cancelButtonColor: '#d33', confirmButtonText: 'Si!'}).then(function () {
                   location.href='tabla?id=<?php echo $f['ticket']?>;'  });"><i class="material-icons">visibility</i></a></td>



               <td><?php echo $ticket = $f['ticket'] ?></td>
               <td><?php echo $f['proveedor'] ?></td>
                 <td><?php echo $RFC = $f['rfc'] ?></td>
               <td><?php echo $f['fecha'] ?></td>
               <td><?php echo "$". number_format($f['importe_iva'], 2); ?></td>
               <!--<td><?php echo $f['num_factura'] ?></td>-->
               <td><?php echo $f['uuid'] ?></td>
               <td><?php echo $f['comentario'] ?></td>
               <td>
                 <script>
                 $("#comentario").click(function(){
                     $.post("comentarioconta.php", $("#form-conta").serialize(), function(data) {
                         alert("Comentario actualizado");
                     });
                 });
                 </script>
                 <form class="form" action="comentarioconta.php" id=form-conta method="post" enctype="multipart/form-data" autocomplete="off">
                   <table>
                 <div class="input-field">
                   <tr>
                     <input type="hidden" name="id_coment" value="<?php echo $f['ticket'] ?>">
                     <input type="text" name="comentario" id="comentario" onblur="may(this.value, this.id)" >

                   </div>
                   <button type="submit"id="btn_guardar">ENVIAR</button>
                 </td>
                 </tr>
                 </table>
                   </form>

               </td>

                     <td><a class="btn-floating btn-medium waves-effect waves-lighten blue" onclick="swal(
                       'Descargando PDF',
                       'Se genero exitosamente',
                       'success'
                       ).then(function () {
                         location.href='descargapdf.php?id=<?php echo $f['ticket']?>;'  });"><i class="material-icons">picture_as_pdf</i></a></td>

                 <td><a class="btn-floating btn-medium waves-effect waves-lighten blue" onclick="swal(
                             'Descargando XML',
                             'Se genero exitosamente',
                             'success'
                             ).then(function () {
                             location.href='descargaxml.php?id=<?php echo $f['ticket']?>;'  });"><i class="material-icons">file_download</i></a></td>

                 <!--Comienza modal de visualizacion-->
                 <td><!--Comienza modal-->

                     <div class="col text-center">
                         <a href="<?php echo $f['ticket'] ?>"  class="btn-floating btn-medium waves-effect waves-lighten deep-blue accent-3" data-toggle="modal" data-target="#basicModal_<?php echo $f['ticket'] ?>"><i class="material-icons">add</i></a>
                     </div>

                     <!-- basic modal -->
                     <div class="modal fade" id="basicModal_<?php echo $f['ticket'] ?>" tabindex="1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                         <div class="modal-content">
                             <div class="modal-header">
                                 <h4 class="modal-title" id="myModalLabel_<?php echo $f['ticket'] ?>">Cuenta del ticket <?php echo $f['ticket'] ?></h4>
                                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                     <span aria-hidden="true">Cerrar <i class="material-icons">close</i></span>
                                 </button>
                             </div>
                             <div class="modal-body">
                                 <table border="1">
                                     <tr>
                                         <!--<td><strong>Fecha registro</strong></td>-->
                                         <td><strong>Cuenta</strong></td>
                                         <td><strong>Monto</strong></td>
                                         <td><strong>Descripcion</strong></td>
                                     </tr>
                                     <?php

                                     $querycon = "SELECT fecha_registro , cuenta , monto , descripcion FROM layouttodo WHERE ticket = '$ticket' AND contador != '0'";
                                     //echo $querycon;
                                     $consultacon = mysqli_query($mysqli, $querycon) or die('Error al buscar en la base de datos.');
                                     $row = mysqli_num_rows($consultacon);
                                     while ( $fila = $consultacon->fetch_assoc() ) {
                                         // $fecha_registro = $fila["fecha_registro"];
                                         $cuenta = $fila['cuenta'];
                                         $monto = $fila['monto'];
                                         $desc = $fila['descripcion'];

                                         ?>
                                         <tr>
                                             <td><h5><?php echo $cuenta; ?></h5></td>
                                             <td><h5><?php echo $monto; ?></h5></td>
                                             <td><h5><?php echo $desc; ?></h5></td>
                                         </tr>
                                     <?php }
                                     if ($row == 0){ ?>
                                         <tr>
                                             <td><h3><?php echo "sin cuenta"; ?></h3></td>
                                             <td><h3><?php echo "contable"; ?></h3></td>
                                             <td><h3><?php echo "cargada"; ?></h3></td>
                                         </tr>
                                     <?php }?>


                                 </table>
                             </div>
                             <table border="1">
                                 <tr>
                                     <!--<td><strong>Fecha registro</strong></td>-->
                                     <td><strong>Comentario</strong></td>
                                 </tr>
                                 <?php

                                 $querycon1 = "SELECT comen_cuen FROM ticket WHERE ticket = '$ticket' AND rfc = '$RFC'";
                                 //echo $querycon1;
                                 $consultacon1 = mysqli_query($mysqli, $querycon1);
                                 $row1 = mysqli_num_rows($consultacon1);
                                 foreach($consultacon1 as $row1)
                                 {
                                     $comen_cuen = $row1['comen_cuen'];
                                 }

                                 ?>
                                 <tr>
                                     <td><h5><?php echo $comen_cuen; ?></h5></td>
                                 </tr>

                             </table>
                             <form action="comenconta.php" method="post">
                                 <input type="text" name="comentarioCuen" id="comentarioCuen" placeholder="Escriba aqui su comentario" autocomplete="off" required>
                                 <input type="hidden" name="id_coment" value="<?php echo $f['ticket'] ?>">
                                 <!--<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>-->
                                 <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                 <div class="modal-footer">
                                     <button type="submit" class="btn btn-primary">Enviar comentario</button>
                             </form>
                         </div>

                     </div>



                 </td>
                 <!--Termina modal-->

               <td>
                 <a href="#" class="btn-floating rbtn-medium waves-effect waves-lighten red" onclick="swal({ title: '¿Estas seguro?',
                 text: '¿desea rechazarlo?', type: 'warning', showCancelButton: true, confirmButtonColor:
                 '#3085d6', cancelButtonColor: '#d33', confirmButtonText: 'Si, rechazar!'}).then(function () {
                 location.href='rechazoconta.php?id=<?php echo $f['ticket'] ?>';  })"><i class="material-icons">clear</i>
               </td>
                <td>
                 <a class="btn-floating btn-medium waves-effect waves-lighten green darken-1 " onclick="swal({ title: 'Valido',
                 text: 'Este archivo esta valido', type: 'success', showCancelButton: true, confirmButtonColor:
                 '#3085d6', cancelButtonColor: '#d33', confirmButtonText: 'Si, Validar!'}).then(function () {
                 location.href='validoconta.php?id=<?php echo $f['ticket'] ?>';  })"><i class="material-icons">check</i></a>
               </td>
               <td><a class="btn-floating btn-medium waves-effect waves-lighten blue" onclick="swal(
                 'Descargando Archivos',
                 'Se genero exitosamente',
                 'success'
                 ).then(function () {
                   location.href='descargarar.php?id=<?php echo $f['ticket']?>'  });"><i class="material-icons">cloud_download</i></a></td>
             </tr>

           <?php } ?>

         </table>
       </div>
       <div>
     </div>
   </div>
 </div>

 </body>
  <?php include '../extend/scripts.php'; ?>
  <script src="../js/validacion.js"></script>

  </html>
