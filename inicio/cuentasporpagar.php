<?php include '../extend/header.php';
  //Restringe a usuarios para no entrar a paginas no permitidas
include '../extend/permiso.php';
include '../conexionEBS/conexion.php';
error_reporting(0);
 ?>

 <!--Buscador en la tabla-->
 <div class="row">
   <div class="col s12" style ="width: 1500px;">
     <nav class="green lighten-1" >
       <div class="nav-wrapper" >
         <div class="input-field">
           <input type="search" id="buscar" autocomplete="off">
           <label for="buscar"><i class="material-icons">search</i></label>
           <i class="material-icons">close</i>
         </div>
       </div>
     </nav>
   </div>
 </div>
 <!-- termina buscador-->
 <?php
 //consulta 5.4
 //$sel = "SELECT ticket,proveedor, rfc , fecha , importe_iva , num_factura , uuid, comentario, estatus FROM ticket WHERE estatus = 'COMPRAS' ";
 //$consulta = mysql_query($sel,$localhost);
 //  $var = mysql_fetch_assoc($consulta) or die ('no se pudo hacer la consulta'.mysql_error());
 //$row = mysql_num_rows($consulta);
 //version 7.3
 $query= "SELECT ticket,proveedor, rfc , fecha , importe_iva , num_factura , uuid, comentario, estatus FROM ticket WHERE estatus = 'COMPRAS' ";
 $consulta = mysqli_query($mysqli, $query) or die('Error al buscar en la base de datos.');
 $row  = mysqli_num_rows($consulta);
  ?>

 <div class="row">
   <div class="col s12" style ="width: 1500px;">
     <div class="card hoverable">
       <div class="card-content">
         <span class="card-title">Archivos pendientes(<?php echo $row ?>)</span>
         <table  class="centered">
           <thead>
             <tr class="cabecera">
               <th>Visualizar</th>
               <th>Ticket</th>
               <th>Proveedor</th>
                 <th>RFC</th>
               <th>Fecha</th>
               <th>Importe_IVA</th>
               <th>UUID</th>
               <th>Comen.anterior</th>
               <th>Comentario</th>
               <th>Pdf</th>
                 <th>XML</th>
               <th>Añadir cuenta</th>
                 <th>Visualizar cuentas</th>
               <th>Rechazado</th>
               <th>Valido</th>
               <th>Descarga archivos</th>
             </tr>
           </thead>

           <?php if($row >= 1){ ?>
           <script>
            Notification.requestPermission();

            function spawnNotification(theBody,theIcon,theTitle) {
              var options = {
                  body: theBody,
                  icon: theIcon
              }
              var n = new Notification(theTitle,options);
              setTimeout(n.close.bind(n), 5000);
            }

            spawnNotification("Tienes facturas por validar pendientes!!", undefined, "Estatus");
           </script>
         <?php }else{ ?>
           <script>
            Notification.requestPermission();

            function spawnNotification(theBody,theIcon,theTitle) {
              var options = {
                  body: theBody,
                  icon: theIcon
              }
              var n = new Notification(theTitle,options);
              setTimeout(n.close.bind(n), 5000);
            }

            spawnNotification("Sin facturas pendientes!!", undefined, "Estatus");
           </script>
       <?php } ?>

           <?php

           while ($f=mysqli_fetch_assoc($consulta)) { ?>
             <tr>
               <td><a class="btn-floating btn-medium waves-effect waves-lighten blue" onclick="swal({ title: 'Visualizar informacion de factura', type: 'info', showCancelButton: true, confirmButtonColor:
               '#3085d6', cancelButtonColor: '#d33', confirmButtonText: 'Si!'}).then(function () {
                   location.href='tablacuentas.php?id=<?php echo $f['ticket']?>;'  });"><i class="material-icons">visibility</i></a></td>
               <td><?php echo $ticket = $f['ticket'] ?></td>
               <td><?php echo $f['proveedor'] ?></td>
               <td><?php echo $f['rfc'] ?>
               <td><?php echo $f['fecha'] ?></td>
               <td><?php echo "$". number_format($f['importe_iva'], 2); ?>
                   <?php $total = $f['importe_iva']; ?></td>
               <!--<td><?php echo $f['num_factura'] ?></td>-->
               <td><?php echo $f['uuid'] ?></td>
               <td><?php echo $f['comentario'] ?></td>
                 <?php  $RFC = $f['rfc']; ?>

               <td>
                 <script>
                 $("#comentario").click(function(){
                     $.post("comentariocuentas.php", $("#form-cuentas").serialize(), function(data) {
                         //alert("Comentario actualizado");
                     });
                 });
                 </script>
                 <form class="form" action="comentariocuentas.php" method="post" id=form-cuentas enctype="multipart/form-data" autocomplete="off">
                   <table>
                 <div class="input-field">
                   <tr>
                     <input type="hidden" name="id_coment" value="<?php echo $f['ticket'] ?>">
                     <input type="text" name="comentario" id="comentario" onblur="may(this.value, this.id)" >

                   </div>
                   <button type="submit"id="btn_guardar">ENVIAR</button>
                 </td>
                 </tr>
                 </table>
                   </form>

               </td>

                     <td><a class="btn-floating btn-medium waves-effect waves-lighten blue" onclick="swal(
                       'Descargando PDF',
                       'Se genero exitosamente',
                       'success'
                       ).then(function () {
                         location.href='descargapdf.php?id=<?php echo $f['ticket']?>;'  });"><i class="material-icons">picture_as_pdf</i></a></td>

                 <td><a class="btn-floating btn-medium waves-effect waves-lighten blue" onclick="swal(
                             'Descargando XML',
                             'Se genero exitosamente',
                             'success'
                             ).then(function () {
                             location.href='descargaxml.php?id=<?php echo $f['ticket']?>;'  });"><i class="material-icons">file_download</i></a></td>
                         <!--<td>
                           <a href="#" class="btn-floating rbtn-medium waves-effect waves-lighten red" onclick="swal({ title: '¿Estas seguro?',
                           text: '¿desea editarlo?', type: 'warning', showCancelButton: true, confirmButtonColor:
                           '#3085d6', cancelButtonColor: '#d33', confirmButtonText: 'Si, editar!'}).then(function () {
                           location.href='editar.php?id=<?php //echo $f['ticket'] ?>';  })"><i class="material-icons">edit</i>
                         </td>-->

                 <td><!--Comienza modal-->
                     <button class="btn-floating btn-medium waves-effect waves-lighten deep-purple accent-3" data-toggle="modal" name="ticket" href="#stack_<?php echo $f['ticket'] ?>" >
                         <i class="large material-icons">touch_app</i></button>


                     <form id="stack_<?php echo $f['ticket'] ?>" class="modal fade" style="zoom: 165%;" tabindex="-1" data-focus-on="input:first" name="cuentasModal" action="back.php" method="post" enctype="multipart/form-data" onblur="may(this.value, this.id)">

                         <div class="modal-header">
                             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                             <h3 align="center">Añada sus cuentas contables</h3>

                         </div>
                         <div class="modal-body">

                             <!--<div class="form-group col-md-2">
                                 <label>Ticket:</label>
                                 <input type="number" class="form-control" name="ticket" id="ticket_<?php echo $f['ticket'];?>" autocomplete="off" style="width: 40px;" value="<?php echo $f['ticket'];?>" required>

                             </div>-->
                             <input type="hidden" class="form-control" name="ticket" id="ticket_<?php echo $f['ticket'];?>" autocomplete="off" style="width: 40px;" value="<?php echo $f['ticket'];?>" required>

                             <div class="form-group col-md-4">
                                 <label>Rfc:</label>
                                 <input type="text" class="form-control" id="rfc_<?php echo $f['ticket'];?>" name="rfc" autocomplete="off" value="<?php echo $RFC;?>"  required>

                             </div>
                             <?php

                             $stid = oci_parse($conn, "SELECT * FROM AP_SUPPLIERS WHERE NUM_1099 = '$RFC'");
                             oci_execute($stid);
                             while (oci_fetch($stid)) {
                                 $numProv = oci_result($stid, 'SEGMENT1');
                             }

                             //oci_free_statement($stid);

                             ?>
                             <div class="form-group col-md-4">
                                 <label for="supplier_num">supplier num:</label>
                                 <input type="text" class="form-control" id="supplier_num_<?php echo $f['ticket'];?>" name="supplier_num" value="<?php echo $numProv; ?>" required>
                             </div>

                             <div class="form-group col-md-4">
                                 <label for="amount">Amount</label>
                                 <input type="text" class="form-control" id="amount_<?php echo $f['ticket'] ?>" name="amount" value="<?php echo $total; ?>" autocomplete="off" required>
                             </div>
                             <div class="form-group col-md-6">
                                 <label for="GLdate">GL Date:</label>
                                 <input type="date" name="GLdate" id="GLdate_<?php echo $f['ticket'] ?>" dateFormat='dd/mm/yyyy' required>
                             </div>
                             <div class="form-group col-md-6">
                                 <label for="termdate">Term Date:</label>
                                 <input type="date" name="termdate" id="GLdate_<?php echo $f['ticket'] ?>" dateFormat='dd/mm/yyyy' required>
                             </div>

                             <div class="form-group col-md-6">
                                 <label>Terms:</label>


                             <select class="browser-default" name='terms' id="terms_<?php echo $f['ticket'] ?>" required style="width:140px; font-size: 8pt;">
                                 <option value="" disabled selected >Opciones</option>
                                 <option value="15 DIAS">15 DIAS</option>
                                 <option value="30 DIAS">30 DIAS</option>
                                 <option value="60 DIAS">60 DIAS</option>
                                 <option value="INMEDIATO">INMEDIATO</option>
                                 <!--<option value="GERENCIA">Gerencia</option>-->
                             </select>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Metodo de pago:</label>
                             </div>
                                 <select class="browser-default" name='Pm' id="Pm_<?php echo $f['ticket'] ?>" required style="width:140px; font-size: 8pt;">
                                     <option value="" disabled selected>Selecciona una opción</option>
                                     <option value="ELECTRÓNICO">ELECTRÓNICO</option>
                                     <option value="CHEQUE">CHEQUE</option>
                                     <option value="CHEQUE EXTERNO">CHEQUE EXTERNO</option>
                                     <option value="MÉTODO DE PAGO">MÉTODO DE PAGO</option>
                                     <option value="COMCHECK">COMCHECK</option>
                                     <option value="TELEGRÁFICO">TELEGRÁFICO</option>
                                     <!--<option value="GERENCIA">Gerencia</option>-->
                                 </select>


                             <!--<label for="FVxml">Ultima Fecha de validacion de factura:</label>
                         <input type="date" name="FVxml" dateFormat: 'dd/mm/yyyy' required>-->
                             <div class="button">
                                 <h6 style=" position: absolute;">Presione para agregar cuenta contable</h6><br><br>
                                 <button type="button" id="add_cuenta_<?php echo $f['ticket'] ?>" class="btn-floating blue" ><i class="material-icons">add_circle</i></button>
                             </div>
                             <!--<button class="btn" data-toggle="modal" href="#stack2">Launch modal</button>-->

                         </div>
                         <div class="modal-footer">
                             <button type="submit" class="btn btn-primary" id="enviar">Enviar</button>
                             <button type="button" data-dismiss="modal" class="btn">Cerrar</button>
                         </div>
                     </form>
       </div>

         <script>
             $(document).ready(function() {
                 $("#add_cuenta_<?php echo $f['ticket'] ?>").click(function(){
                     var contador = $("input[type='text']").length;
                     var maxField = 1; //Input fields increment limitation


                     $(this).before('<div class="form-group col-md-3"><label for="numero'+ contador +'">Cuenta contable:</label><input type="text" id="numero'+ contador +'" name="numero[]"required/><label for="monto'+ contador +'">Monto:</label><input type="text" id="monto'+ contador +'" name="monto[]" autocomplete="off" required/><label for="descripcion'+ contador +'">descripcion:</label><input type="text" id="descripcion'+ contador +'" name="descripcion[]"required/><br><label for="delete_cuenta'+ contador +'">Borrar cuenta.:</label><button type="button" class="delete_cuenta btn-floating blue"><i class="material-icons">delete</i></button></div>');

                 });

                 $(document).on('click', '.delete_cuenta', function(){
                     $(this).parent().remove();
                 });
             });</script>
         <!--mandar datos de modal a php-->

         <!--Script para que funcione el boton de de consultar fechas-->
         <script>
             $('#nick').change(function() {
                 $.post('ConsultaFechasRfc.php', {
                     nick: $('#nick').val(),
                     beforeSend: function() {
                         $('.validacion').html("<p>Espere un momento por favor..</p>");
                     }
                 }, function(respuesta) {
                     $('.validacion').html(respuesta);
                 });
             });
         </script>
                 </td>

         <!--Comienza modal de visualizacion-->
         <td><!--Comienza modal-->

                     <div class="col text-center">
                         <a href="<?php echo $f['ticket'] ?>"  class="btn-floating btn-medium waves-effect waves-lighten deep-blue accent-3" data-toggle="modal" data-target="#basicModal_<?php echo $f['ticket'] ?>"><i class="material-icons">add</i></a>
                     </div>

             <!-- basic modal -->
             <div class="modal fade" id="basicModal_<?php echo $f['ticket'] ?>" tabindex="1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                     <div class="modal-content">
                         <div class="modal-header">
                             <h4 class="modal-title" id="myModalLabel_<?php echo $f['ticket'] ?>">Cuenta del ticket <?php echo $f['ticket'] ?></h4>
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">Cerrar <i class="material-icons">close</i></span>
                             </button>
                         </div>
                         <div class="modal-body">
                             <table border="1">
                                 <tr>
                                     <!--<td><strong>Fecha registro</strong></td>-->
                                     <td><strong>Cuenta</strong></td>
                                     <td><strong>Monto</strong></td>
                                     <td><strong>Descripcion</strong></td>
                                 </tr>
                                 <?php

                                 $querycon = "SELECT fecha_registro , cuenta , monto , descripcion FROM layouttodo WHERE ticket = '$ticket' AND contador != '0'";
                                 //echo $querycon;
                                 $consultacon = mysqli_query($mysqli, $querycon) or die('Error al buscar en la base de datos.');
                                 $row = mysqli_num_rows($consultacon);
                                 while ( $fila = $consultacon->fetch_assoc() ) {
                                    // $fecha_registro = $fila["fecha_registro"];
                                     $cuenta = $fila['cuenta'];
                                     $monto = $fila['monto'];
                                     $desc = $fila['descripcion'];

                                     ?>
                                     <tr>
                                         <td><h5><?php echo $cuenta; ?></h5></td>
                                        <td><h5><?php echo $monto; ?></h5></td>
                                         <td><h5><?php echo $desc; ?></h5></td>
                                 </tr>
                               <?php }
                                 if ($row == 0){ ?>
                                     <tr>
                                         <td><h3><?php echo "sin cuenta"; ?></h3></td>
                                         <td><h3><?php echo "contable"; ?></h3></td>
                                         <td><h3><?php echo "cargada"; ?></h3></td>
                                     </tr>
                                 <?php }?>


                             </table>
                         </div>
                         <table border="1">
                             <tr>
                                 <!--<td><strong>Fecha registro</strong></td>-->
                                 <td><strong>Comentario</strong></td>
                             </tr>
                             <?php

                             $querycon1 = "SELECT comen_cuen FROM ticket WHERE ticket = '$ticket' AND rfc = '$RFC'";
                                 //echo $querycon1;
                                 $consultacon1 = mysqli_query($mysqli, $querycon1);
                                 $row1 = mysqli_num_rows($consultacon1);
                                foreach($consultacon1 as $row1)
                                 {
                                 $comen_cuen = $row1['comen_cuen'];
                                 }

                                ?>
                                 <tr>
                                         <td><h5><?php echo $comen_cuen; ?></h5></td>
                                 </tr>

                         </table>
                             <form action="comencuen.php" method="post">
                             <input type="text" name="comentarioCuen" id="comentarioCuen" placeholder="Escriba aqui su comentario" autocomplete="off" required>
                                 <input type="hidden" name="id_coment" value="<?php echo $f['ticket'] ?>">
                             <!--<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>-->
                             <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                         <div class="modal-footer">
                             <button type="submit" class="btn btn-primary">Enviar comentario</button>
                             </form>
                         </div>

                     </div>



       </td>
<!--Termina modal-->

               <td>
                 <a href="#" class="btn-floating rbtn-medium waves-effect waves-lighten red" onclick="swal({ title: '¿Estas seguro?',
                 text: '¿desea rechazarlo?', type: 'warning', showCancelButton: true, confirmButtonColor:
                 '#3085d6', cancelButtonColor: '#d33', confirmButtonText: 'Si, rechazar!'}).then(function () {
                 location.href='rechazocuentas.php?id=<?php echo $f['ticket'] ?>';  })"><i class="material-icons">clear</i>
               </td>
               <td>
                 <a class="btn-floating btn-medium waves-effect waves-lighten green darken-1 " onclick="swal({ title: 'Valido',
                 text: 'Este archivo esta valido', type: 'success', showCancelButton: true, confirmButtonColor:
                 '#3085d6', cancelButtonColor: '#d33', confirmButtonText: 'Si, Validar!'}).then(function () {
                 location.href='validocuentas.php?id=<?php echo $f['ticket'] ?>';  })"><i class="material-icons">check</i></a>
               </td>
               <td><a class="btn-floating btn-medium waves-effect waves-lighten blue" onclick="swal(
                 'Descargando Archivos',
                 'Se genero exitosamente',
                 'success'
                 ).then(function () {
                   location.href='descargarar.php?id=<?php echo $f['ticket']?>'  });"><i class="material-icons">cloud_download</i></a></td>
             </tr>
           <?php } ?>

         </table>
       </div>
       <div>
     </div>
   </div>
 </div>

<!--comienza segunda card-->
 <!--Buscador en la tabla-->
 <div class="row">
   <div class="col s12" style ="width: 1500px;">
     <nav class="green lighten-1" >
       <div class="nav-wrapper" >
         <div class="input-field">
           <input type="search" id="buscar" autocomplete="off">
           <label for="buscar"><i class="material-icons">search</i></label>
           <i class="material-icons">close</i>
         </div>
       </div>
     </nav>
   </div>
 </div>
 <!-- termina buscador-->
 <?php
 //consulta 5.4
 //$sel = "¡SELECT ticket,proveedor, rfc , fecha , importe_iva , num_factura , uuid, comentario, estatus FROM ticket WHERE estatus = 'CONTABILIDADD' ";
 //$consulta = mysql_query($sel,$localhost);
 //  $var = mysql_fetch_assoc($consulta) or die ('no se pudo hacer la consulta'.mysql_error());
 //$row = mysql_num_rows($consulta);
//version 7.3
 $query= "SELECT ticket,proveedor, rfc , fecha , importe_iva , num_factura , uuid, comentario, estatus FROM ticket WHERE estatus = 'CONTABILIDADD' ";
 $consulta = mysqli_query($mysqli, $query) or die('Error al buscar en la base de datos.');
 $row = mysqli_num_rows($consulta);
  ?>

 <div class="row">
   <div class="col s12" style ="width: 1500px;">
     <div class="card hoverable">
       <div class="card-content">
         <span class="card-title">Archivos devueltos:(<?php echo $row ?>)</span>
         <table  class="centered">
           <thead>
             <tr class="cabecera">
               <th>Visualizar</th>
               <th>Ticket</th>
               <th>Proveedor</th>
                 <th>RFC</th>
               <th>Fecha</th>
               <th>Importe_IVA</th>
               <th>UUID</th>
               <th>Comen.anterior</th>
               <th>Comentario</th>
               <th>Pdf</th>
                 <th>XML</th>
                 <th>Añadir cuenta</th>
                 <th>Visualizar cuentas</th>
               <th>Rechazado</th>
               <th>Valido</th>
               <th>Descarga archivos</th>
             </tr>
           </thead>
           <?php

           while ($f=mysqli_fetch_assoc($consulta)) { ?>
             <tr>
               <td><a class="btn-floating btn-medium waves-effect waves-lighten blue" onclick="swal({ title: 'Visualizar informacion de factura', type: 'info', showCancelButton: true, confirmButtonColor:
               '#3085d6', cancelButtonColor: '#d33', confirmButtonText: 'Si!'}).then(function () {
                   location.href='tablacuentas?id=<?php echo $f['ticket']?>;'  });"><i class="material-icons">visibility</i></a></td>
               <td><?php echo $ticketD = $f['ticket'] ?></td>
               <td><?php echo $f['proveedor'] ?></td>
               <td><?php echo $RFCD = $f['rfc'] ?>
               <td><?php echo $f['fecha'] ?></td>
               <td><?php echo "$". number_format($f['importe_iva'], 2); ?></td>
               <!--<td><?php echo $f['num_factura'] ?></td>-->
               <td><?php echo $f['uuid'] ?></td>
               <td><?php echo $f['comentario'] ?></td>
                 <?php $totalD = $f['importe_iva']; ?>
               <td>
                 <script>
                 $("#comentario").click(function(){
                     $.post("comentariocuentas.php", $("#form-cuentasd").serialize(), function(data) {
                         //alert("Comentario actualizado");
                     });
                 });
                 </script>
                 <form class="form" action="comentariocuentas.php" id="form-cuentasd" method="post" enctype="multipart/form-data" autocomplete="off">
                   <table>
                 <div class="input-field">
                   <tr>
                     <input type="hidden" name="id_coment" value="<?php echo $f['ticket'] ?>">
                     <input type="text" name="comentario" id="comentario" onblur="may(this.value, this.id)" >

                   </div>
                   <button type="submit" id="btn_guardar">ENVIAR</button>
                 </td>
                 </tr>
                 </table>
                   </form>

               </td>

                     <td><a class="btn-floating btn-medium waves-effect waves-lighten blue" onclick="swal(
                       'Descargando PDF',
                       'Se genero exitosamente',
                       'success'
                       ).then(function () {
                         location.href='descargapdf.php?id=<?php echo $f['ticket']?>;'  });"><i class="material-icons">picture_as_pdf</i></a></td>

                 <td><a class="btn-floating btn-medium waves-effect waves-lighten blue" onclick="swal(
                             'Descargando XML',
                             'Se genero exitosamente',
                             'success'
                             ).then(function () {
                             location.href='descargaxml.php?id=<?php echo $f['ticket']?>;'  });"><i class="material-icons">file_download</i></a></td>

                 <td><!--Comienza modal-->
                     <button class="btn-floating btn-medium waves-effect waves-lighten deep-purple accent-3" data-toggle="modal" name="ticket" href="#stackD_<?php echo $ticketD ?>" >
                         <i class="large material-icons">touch_app</i></button>


                     <form id="stackD_<?php echo $ticketD ?>" class="modal fade" style="zoom: 165%;" tabindex="-1" data-focus-on="input:first" name="cuentasModal" action="back.php" method="post" enctype="multipart/form-data" onblur="may(this.value, this.id)">

                         <div class="modal-header">
                             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                             <h3 align="center">Añada sus cuentas contables</h3>

                         </div>
                         <div class="modal-body">

                             <!--<div class="form-group col-md-2">
                                 <label>Ticket:</label>
                                 <input type="number" class="form-control" name="ticket" id="ticket_<?php echo $ticketD;?>" autocomplete="off" style="width: 40px;" value="<?php echo $ticketD;?>" required>

                             </div>-->
                             <input type="hidden" class="form-control" name="ticket" id="ticket_<?php echo $ticketD;?>" autocomplete="off" style="width: 40px;" value="<?php echo $ticketD;?>" required>

                             <div class="form-group col-md-4">
                                 <label>Rfc:</label>
                                 <input type="text" class="form-control" id="rfc_<?php echo $ticketD;?>" name="rfc" autocomplete="off" value="<?php echo $RFCD;?>"  required>

                             </div>
                             <?php

                             $stid = oci_parse($conn, "SELECT * FROM AP_SUPPLIERS WHERE NUM_1099 = '$RFCD'");
                             oci_execute($stid);
                             while (oci_fetch($stid)) {
                                 $numProv = oci_result($stid, 'SEGMENT1');
                             }

                             //oci_free_statement($stid);

                             ?>
                             <div class="form-group col-md-4">
                                 <label for="supplier_num">supplier num:</label>
                                 <input type="text" class="form-control" id="supplier_num_<?php echo $ticketD;?>" name="supplier_num" value="<?php echo $numProv; ?>" required>
                             </div>

                             <div class="form-group col-md-4">
                                 <label for="amount">Amount</label>
                                 <input type="text" class="form-control" id="amount_<?php echo $ticketD ?>" name="amount" value="<?php echo $totalD; ?>" autocomplete="off" required>
                             </div>
                             <div class="form-group col-md-6">
                                 <label for="GLdate">GL Date:</label>
                                 <input type="date" name="GLdate" id="GLdate_<?php echo $ticketD ?>" dateFormat='dd/mm/yyyy' required>
                             </div>
                             <div class="form-group col-md-6">
                                 <label for="termdate">Term Date:</label>
                                 <input type="date" name="termdate" id="GLdate_<?php echo $ticketD?>" dateFormat='dd/mm/yyyy' required>
                             </div>

                             <div class="form-group col-md-6">
                                 <label>Terms:</label>


                                 <select class="browser-default" name='terms' id="terms_<?php echo $ticketD ?>" required style="width:140px; font-size: 8pt;">
                                     <option value="" disabled selected >Opciones</option>
                                     <option value="15 DIAS">15 DIAS</option>
                                     <option value="30 DIAS">30 DIAS</option>
                                     <option value="60 DIAS">60 DIAS</option>
                                     <option value="INMEDIATO">INMEDIATO</option>
                                     <!--<option value="GERENCIA">Gerencia</option>-->
                                 </select>
                             </div>
                             <div class="form-group col-md-6">
                                 <label>Metodo de pago:</label>
                             </div>
                             <select class="browser-default" name='Pm' id="Pm_<?php echo $ticketD ?>" required style="width:140px; font-size: 8pt;">
                                 <option value="" disabled selected>Selecciona una opción</option>
                                 <option value="ELECTRÓNICO">ELECTRÓNICO</option>
                                 <option value="CHEQUE">CHEQUE</option>
                                 <option value="CHEQUE EXTERNO">CHEQUE EXTERNO</option>
                                 <option value="MÉTODO DE PAGO">MÉTODO DE PAGO</option>
                                 <option value="COMCHECK">COMCHECK</option>
                                 <option value="TELEGRÁFICO">TELEGRÁFICO</option>
                                 <!--<option value="GERENCIA">Gerencia</option>-->
                             </select>


                             <!--<label for="FVxml">Ultima Fecha de validacion de factura:</label>
                         <input type="date" name="FVxml" dateFormat: 'dd/mm/yyyy' required>-->
                             <div class="button">
                                 <h6 style=" position: absolute;">Presione para agregar cuenta contable</h6><br><br>
                                 <button type="button" id="add_cuentaD_<?php echo $ticketD ?>" class="btn-floating blue" ><i class="material-icons">add_circle</i></button>
                             </div>
                             <!--<button class="btn" data-toggle="modal" href="#stack2">Launch modal</button>-->

                         </div>
                         <div class="modal-footer">
                             <button type="submit" class="btn btn-primary" id="enviar">Enviar</button>
                             <button type="button" data-dismiss="modal" class="btn">Cerrar</button>
                         </div>
                     </form>
       </div>

         <script>
             $(document).ready(function() {
                 $("#add_cuentaD_<?php echo $ticketD ?>").click(function(){
                     var contador = $("input[type='text']").length;
                     var maxField = 1; //Input fields increment limitation


                     $(this).before('<div class="form-group col-md-3"><label for="numero'+ contador +'">Cuenta contable:</label><input type="text" id="numero'+ contador +'" name="numero[]"required/><label for="monto'+ contador +'">Monto:</label><input type="text" id="monto'+ contador +'" name="monto[]" autocomplete="off" required/><label for="descripcion'+ contador +'">descripcion:</label><input type="text" id="descripcion'+ contador +'" name="descripcion[]"required/><br><label for="delete_cuenta'+ contador +'">Borrar cuenta.:</label><button type="button" class="delete_cuenta btn-floating blue"><i class="material-icons">delete</i></button></div>');

                 });

                 $(document).on('click', '.delete_cuenta', function(){
                     $(this).parent().remove();
                 });
             });</script>
         <!--mandar datos de modal a php-->

         <!--Script para que funcione el boton de de consultar fechas-->
         <script>
             $('#nick').change(function() {
                 $.post('ConsultaFechasRfc.php', {
                     nick: $('#nick').val(),
                     beforeSend: function() {
                         $('.validacion').html("<p>Espere un momento por favor..</p>");
                     }
                 }, function(respuesta) {
                     $('.validacion').html(respuesta);
                 });
             });
         </script>
         </td>

         <!--Comienza modal de visualizacion-->
         <td><!--Comienza modal-->

             <div class="col text-center">
                 <a href="<?php echo $ticketD ?>"  class="btn-floating btn-medium waves-effect waves-lighten deep-blue accent-3" data-toggle="modal" data-target="#basicModalD_<?php echo $ticketD ?>"><i class="material-icons">add</i></a>
             </div>

             <!-- basic modal -->
             <div class="modal fade" id="basicModalD_<?php echo $ticketD ?>" tabindex="1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
                 <div class="modal-content">
                     <div class="modal-header">
                         <h4 class="modal-title" id="myModalLabel_<?php echo $ticketD ?>">Cuenta del ticket <?php echo $ticketD ?></h4>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">Cerrar <i class="material-icons">close</i></span>
                         </button>
                     </div>
                     <div class="modal-body">
                         <table border="1">
                             <tr>
                                 <!--<td><strong>Fecha registro</strong></td>-->
                                 <td><strong>Cuenta</strong></td>
                                 <td><strong>Monto</strong></td>
                                 <td><strong>Descripcion</strong></td>
                             </tr>
                             <?php

                             $querycon = "SELECT fecha_registro , cuenta , monto , descripcion FROM layouttodo WHERE ticket = '$ticketD' AND contador != '0'";
                             //echo $querycon;
                             $consultacon = mysqli_query($mysqli, $querycon) or die('Error al buscar en la base de datos.');
                             $row = mysqli_num_rows($consultacon);
                             while ( $fila = $consultacon->fetch_assoc() ) {
                                 // $fecha_registro = $fila["fecha_registro"];
                                 $cuenta = $fila['cuenta'];
                                 $monto = $fila['monto'];
                                 $desc = $fila['descripcion'];

                                 ?>
                                 <tr>
                                     <td><h5><?php echo $cuenta; ?></h5></td>
                                     <td><h5><?php echo $monto; ?></h5></td>
                                     <td><h5><?php echo $desc; ?></h5></td>
                                 </tr>
                             <?php }
                             if ($row == 0){ ?>
                                 <tr>
                                     <td><h3><?php echo "sin cuenta"; ?></h3></td>
                                     <td><h3><?php echo "contable"; ?></h3></td>
                                     <td><h3><?php echo "cargada"; ?></h3></td>
                                 </tr>
                             <?php }?>


                         </table>
                     </div>
                     <table border="1">
                         <tr>
                             <!--<td><strong>Fecha registro</strong></td>-->
                             <td><strong>Comentario</strong></td>
                         </tr>
                         <?php

                         $querycon1 = "SELECT comen_cuen FROM ticket WHERE ticket = '$ticketD' AND rfc = '$RFCD'";
                         //echo $querycon1;
                         $consultacon1 = mysqli_query($mysqli, $querycon1);
                         $row1 = mysqli_num_rows($consultacon1);
                         foreach($consultacon1 as $row1)
                         {
                             $comen_cuen = $row1['comen_cuen'];
                         }

                         ?>
                         <tr>
                             <td><h5><?php echo $comen_cuen; ?></h5></td>
                         </tr>

                     </table>
                     <form action="comencuen.php" method="post">
                         <input type="text" name="comentarioCuen" id="comentarioCuen" placeholder="Escriba aqui su comentario" autocomplete="off" required>
                         <input type="hidden" name="id_coment" value="<?php echo $ticketD ?>">
                         <!--<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>-->
                         <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                         <div class="modal-footer">
                             <button type="submit" class="btn btn-primary">Enviar comentario</button>
                     </form>
                 </div>

             </div>



         </td>
         <!--Termina modal-->


               <td>
                 <a href="#" class="btn-floating rbtn-medium waves-effect waves-lighten red" onclick="swal({ title: '¿Estas seguro?',
                 text: '¿desea rechazarlo?', type: 'warning', showCancelButton: true, confirmButtonColor:
                 '#3085d6', cancelButtonColor: '#d33', confirmButtonText: 'Si, rechazar!'}).then(function () {
                 location.href='rechazocuentas.php?id=<?php echo $f['ticket'] ?>';  })"><i class="material-icons">clear</i>
               </td>
               <td>
                 <a class="btn-floating btn-medium waves-effect waves-lighten green darken-1 " onclick="swal({ title: 'Valido',
                 text: 'Este archivo esta valido', type: 'success', showCancelButton: true, confirmButtonColor:
                 '#3085d6', cancelButtonColor: '#d33', confirmButtonText: 'Si, Validar!'}).then(function () {
                 location.href='validocuentas.php?id=<?php echo $f['ticket'] ?>';  })"><i class="material-icons">check</i></a>
               </td>
               <td><a class="btn-floating btn-medium waves-effect waves-lighten blue" onclick="swal(
                 'Descargando Archivos',
                 'Se genero exitosamente',
                 'success'
                 ).then(function () {
                   location.href='descargarar.php?id=<?php echo $f['ticket']?>'  });"><i class="material-icons">cloud_download</i></a></td>
             </tr>
           <?php } ?>

         </table>
       </div>
       <div>
     </div>
   </div>
 </div>


 </body>
  <?php include '../extend/scripts.php'; ?>
  <script src="../js/validacion.js"></script>

  </html>
