<?php
include '../Conexion/conexion.php';
require 'lib_composer/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$fechaactual = getdate();
$hoy = date("j/n/Y");
$usuarios = $_POST['fecha'];
//echo $usuarios;

//Empieza consulta
$sel = "SELECT ticket,supplier_num , amount , gl_date , terms_date , terms , forma_pago , uuid , description , invoice_date , fecha_registro FROM layouttodo WHERE fecha_final = '".$usuarios."' AND uuid IS NOT NULL AND Cargado =1";

$consulta = mysqli_query($mysqli, $sel) or die('Error al buscar en la base de datos.');
$row = mysqli_num_rows($consulta);

if($row > 0) {
    $documento = new Spreadsheet();
    $documento
        ->getProperties()
        ->setCreator("Aquí va el creador, como cadena")
        ->setLastModifiedBy('Parzibyte')// última vez modificado por
        ->setTitle('Mi primer documento creado con PhpSpreadSheet')
        ->setSubject('El asunto')
        ->setDescription('Este documento fue generado para parzibyte.me')
        ->setKeywords('etiquetas o palabras clave separadas por espacios')
        ->setCategory('La categoría');

    $nombreDelDocumento = "dataload'.$usuarios.'.xlsx";
    /**
     * Los siguientes encabezados son necesarios para que
     * el navegador entienda que no le estamos mandando
     * simple HTML
     * Por cierto: no hagas ningún echo ni cosas de esas; es decir, no imprimas nada
     */
    $w = 0;
    $e =0;
    $i = 1;
    $p = $e + $i;

    //SE agregan los datos
    //Numero de fila donde se va a comenzar a rellenar

    while($fila=mysqli_fetch_array($consulta)){
        $ticket = $fila['ticket'];
        $documento->setActiveSheetIndex(0)
            ->setCellValue('A'.$p, "TAB")
            ->setCellValue('B'.$p, "TAB")
            ->setCellValue('C'.$p, "TAB")
            ->setCellValue('D'.$p, "TAB")
            ->setCellValue('E'.$p, $fila['supplier_num'])
            ->setCellValue('F'.$p, "TAB")
            ->setCellValue('G'.$p, "TAB")
            ->setCellValue('H'.$p, $fila['invoice_date'])
            ->setCellValue('I'.$p, "TAB")
            ->setCellValue('J'.$p, $fila['uuid'])
            ->setCellValue('K'.$p, "TAB")
            ->setCellValue('L'.$p, "TAB")
            ->setCellValue('M'.$p, $fila['amount'])
            ->setCellValue('N'.$p, "TAB")
            ->setCellValue('O'.$p, "TAB")
            ->setCellValue('P'.$p, "TAB")
            ->setCellValue('Q'.$p, "TAB")
            ->setCellValue('R'.$p, $fila['gl_date'])
            ->setCellValue('S'.$p, "TAB")
            ->setCellValue('T'.$p, "TAB")
            ->setCellValue('U'.$p, "TAB")
            ->setCellValue('V'.$p, $fila['description'])
            ->setCellValue('W'.$p, "TAB")
            ->setCellValue('X'.$p, "TAB")
            ->setCellValue('Y'.$p, $fila['terms_date'])
            ->setCellValue('Z'.$p, "TAB")
            ->setCellValue('AA'.$p, $fila['terms'])
            ->setCellValue('AB'.$p, "TAB")
            ->setCellValue('AC'.$p, $fila['forma_pago'])
            ->setCellValue('AD'.$p, "*ML(296,328)")
            ->setCellValue('AE'.$p, "*ML(59,371)")
            ->setCellValue('AF'.$p, "*DL(758,479)")
            ->setCellValue('AG'.$p, "*ML(172,446)");

        $selCuen = "SELECT count(*) cuenta FROM layouttodo WHERE ticket = '".$ticket."' AND fecha_final = '".$usuarios."' AND cuenta IS NOT NULL";
        $result = mysqli_query($mysqli , $selCuen);
        while($fe=mysqli_fetch_array($result)){
            $numero = $fe['cuenta'];
        }

        $sel1 = "SELECT cuenta, monto, descripcion FROM layouttodo WHERE fecha_final = '".$usuarios."' AND ticket = '".$ticket."' AND descripcion IS NOT NULL";
        $consulta1 = mysqli_query($mysqli, $sel1);

        while($fii=mysqli_fetch_array($consulta1)){
            $row1 = mysqli_num_rows($consulta1);

            $documento->setActiveSheetIndex(0)->setCellValue('AH'.$p, $fii['monto']) //setting value by column and row indexes if needed
            ->setCellValue('AI'.$p, "TAB")
                ->setCellValue('AJ'.$p, "OCE_AP_IVA_16%") //setting value by column and row indexes if needed
                ->setCellValue('AK'.$p, "TAB")
                ->setCellValue('AL'.$p, $fii['cuenta']) //setting value by column and row indexes if needed
                ->setCellValue('AM'.$p, "TAB")
                ->setCellValue('AN'.$p, $fii['descripcion']) //setting value by column and row indexes if needed
                ->setCellValue('AO'.$p, "TAB") //setting value by column and row indexes if needed
                ->setCellValue('AP'.$p, "*NR") //setting value by column and row indexes if needed
                ->setCellValue('AQ'.$p, "TAB")
                ->setCellValue('AR'.$p, "TAB");

            $p++;
            $w++;

        }//Termina while consulta 1

        $v = $p - 1;
        $sel12 = "SELECT uno,dos,tres FROM layouttodo WHERE fecha_final = '".$usuarios."' AND ticket = '".$ticket."'";
        $consulta12 = mysqli_query($mysqli,$sel12);
        while($fii2=mysqli_fetch_array($consulta12)){
            $documento->setActiveSheetIndex(0)

                ->setCellValue('AP'.$v, $fii2['uno'])
                ->setCellValue('AQ'.$v, $fii2['dos'])
                ->setCellValue('AR'.$v, $fii2['tres']);

        }

        if($v <= $p) {
            $sel123 = "SELECT cuenta, monto, descripcion, CONTADOR, ticket
          FROM layouttodo WHERE ticket =  '".$ticket."' AND CONTADOR = ( SELECT CONTADOR FROM layouttodo ORDER BY CONTADOR DESC LIMIT 1 ) AND fecha_final =  '".$usuarios."' AND descripcion IS NOT NULL LIMIT 1";
            $consulta123 = mysqli_query($mysqli,$sel123);
            while($fii23=mysqli_fetch_array($consulta123)){
                $documento->setActiveSheetIndex(0)
                    ->setCellValue('AH'.$v, $fii23['monto']) //setting value by column and row indexes if needed
                    ->setCellValue('AI'.$v, "TAB")
                    ->setCellValue('AJ'.$v, "OCE_AP_IVA_16%") //setting value by column and row indexes if needed
                    ->setCellValue('AK'.$v, "TAB")
                    ->setCellValue('AL'.$v, $fii23['cuenta']) //setting value by column and row indexes if needed
                    ->setCellValue('AM'.$v, "TAB")
                    ->setCellValue('AN'.$v, $fii23['descripcion']) //setting value by column and row indexes if needed
                    ->setCellValue('AO'.$v, "TAB");
            }
        }



        $i++;
        $update = "UPDATE layouttodo SET layouttodo.Cargado = 0 WHERE layouttodo.ticket = ".$ticket;
        mysqli_query($mysqli, $update);
    }//Termina consulta

    $documento->getActiveSheet()->getProtection()->setSheet(true);
    $documento->getSecurity()->setLockWindows(true);
    $documento->getSecurity()->setLockStructure(true);
    $documento->getSecurity()->setWorkbookPassword("Central2019");

// Redirect output to a client’s web browser (Xlsx)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Dataload_'. $hoy . '.xlsx"');
    header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $writer = IOFactory::createWriter($documento, 'Xlsx');
    $writer->save('php://output');
    exit;
}else{
    echo "no hay resultados para mostrar";
}
?>