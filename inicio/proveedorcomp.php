<?php
include '../extend/header.php';
  //Restringe a usuarios para no entrar a paginas no permitidas
//include '../extend/permiso.php';
include '../Conexion/conexion.php';
//include '../conexionEBS/conexion.php';

 ?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class = 'row'>
  <div class = 'col s8 m8'>
    <div class = 'card hoverable'>
<h3 align="center">Busqueda de proveedor</h3>
<!-- busqueda por prerecibo-->
<h3>BUSQUEDA POR RFC</h3>
    <form class="form" action="busquedarfcEBS.php" method="post" >
        <div class="input-field col l5 m6 s12">
      <!--<input type="text" id="start_fecha" name="start_fecha" value="dd/mm/aaaa">
      <input type="text" id="end_fecha" name="end_fecha" value="dd/mm/aaaa">
      <input type="hidden" id="fomr_sent" name="form_sent" value="true">-->
      <input type="text" name="rfcebs" required>
      <label for="rfcebs">RFC</label>
    </div>
    <div class="input-field col l2 m6 s12">
      <button type="submit" name="name" value="" class="btn black" id="btn_search"><i class="material-icons">search</i>Buscar</button>
</div>
    <!--
    <input type="text" name="fecha" placeholder="Ingrese fecha a buscar con el siguiente formato: (dd-mm-aa)">
      <button type="submit" name="name" value="" class="btn black" id="btn_search"><i class="material-icons">search</i></button>
    -->
</form>

<!--cONTRARECIBO-->
<h3>BUSQUEDA POR FOLIO DE PROVEDOR</h3>
    <form class="form" action="busquedaVidEBS.php" method="post" >
      <div class="input-field col l5 m6 s12">
      <!--<input type="text" id="start_fecha" name="start_fecha" value="dd/mm/aaaa">
      <input type="text" id="end_fecha" name="end_fecha" value="dd/mm/aaaa">
      <input type="hidden" id="fomr_sent" name="form_sent" value="true">-->
      <label for="Videbs">FOLIO</label>
      <input type="text" name="Videbs" required>
    </div>
        <div class="input-field col l2 m6 s12">
      <button type="submit" name="name" value="" class="btn black" id="btn_search"><i class="material-icons">search</i>Buscar</button>
    </div>
</form>

        <!--Cuenta contables para wsdl-->
<!--<span>BUSQUEDA DE CUENTA CONTABLE</span>
<span style="padding-left:600px;">PERIODO: MES/AÑO (LOS ULTIMOS DOS NUMEROS DEL AÑO)</span>
    <form class="form" action="busquedacuenta.php" method="post" >
      <div class="input-field col l5 m2 s2">
      <label for="contrarecibo">EJEMPLO = 01.00.0000.1102.00000.00.00.0000</label>
      <input type="text" name="contrarecibo"  required>
    </div>
    <div class="input-field col l2 m2 s2">
      <select class="browser-default" name="mes" style="width:120px" required>
        <option></option>
        <option value="ENE">ENERO</option>
        <option value="FEB">FEBRERO</option>
        <option value="MAR">MARZO</option>
        <option value="ABR">ABRIL</option>
        <option value="MAY">MAYO</option>
        <option value="JUN">JUNIO</option>
        <option value="JUL">JULIO</option>
        <option value="AGO">AGOSTO</option>
        <option value="SEP">SEPTIEMBRE</option>
        <option value="OCT">OCTUBRE</option>
        <option value="NOV">NOVIEMBRE</option>
        <option value="DIC">DICIEMBRE</option>
      </select>
</div>
<div class="input-field col l2 m2 s2">
  <label for="ano">AÑO (Ej.18)</label>
<input type="text" name="ano" style="width:120px" required>
</div>
        <div class="input-field col l2 m6 s12">
      <button type="submit" name="name" value="" class="btn black" id="btn_search"><i class="material-icons">search</i>Buscar</button>
    </div>
</form>-->
        <style>
            #muestra_datos1{
                display: inline;
                width: 100px;
                height: 200px;
            }
        </style>

        <h3>BUSQUEDA DE CUENTA CONTABLE <label><input type="checkbox" id="enable-option" class="filled-in" style="position: initial;left: -9999px; opacity: 1; width: 25px; height: 14px"></label></h3>

        <form class="form" action="busquedacuenta.php" method="POST" >
            <div class="input-field col l2 m2 s2">
                <!--<label for="contrarecibo">EJEMPLO = 01.00.0000.1102.00000.00.00.0000</label>-->
                <!--<label for="seg1">Segmento 1</label>-->
                <input type="number" name="seg1" id="seg1" min="0" placeholder="Segmento 1" required style="display:none;">
                <div id="muestra_datos1"></div>
            </div>
            <div class="input-field col l2 m2 s2">
                <!--<label for="contrarecibo">EJEMPLO = 01.00.0000.1102.00000.00.00.0000</label>-->
                <input type="number" name="seg2" id="seg2" min="0" placeholder="Segmento 2" required style="display:none;">
                <div id="muestra_datos2"></div>
            </div>
            <div class="input-field col l2 m2 s2">
                <!--<label for="contrarecibo">EJEMPLO = 01.00.0000.1102.00000.00.00.0000</label>-->
                <input type="number" name="seg3" min="0" id="seg3" placeholder="Segmento 3" required style="display:none;">
                <div id="muestra_datos3"></div>
            </div>
            <div class="input-field col l2 m2 s2">
                <!--<label for="contrarecibo">EJEMPLO = 01.00.0000.1102.00000.00.00.0000</label>-->
                <input type="number" name="seg4" min="0" id="seg4" placeholder="Segmento 4"  required style="display:none;">
                <div id="muestra_datos4"></div>
            </div>

            <div class="input-field col l2 m2 s2" style="display:none;" id="periodos">
                <h6>PERIODO: MES/AÑO (LOS ULTIMOS DOS NUMEROS DEL AÑO)</h6>
                <select class="browser-default" name="mes" id="mes" style="width:120px" required>
                    <option></option>
                    <option value="ENE">ENERO</option>
                    <option value="FEB">FEBRERO</option>
                    <option value="MAR">MARZO</option>
                    <option value="ABR">ABRIL</option>
                    <option value="MAY">MAYO</option>
                    <option value="JUN">JUNIO</option>
                    <option value="JUL">JULIO</option>
                    <option value="AGO">AGOSTO</option>
                    <option value="SEP">SEPTIEMBRE</option>
                    <option value="OCT">OCTUBRE</option>
                    <option value="NOV">NOVIEMBRE</option>
                    <option value="DIC">DICIEMBRE</option>
                </select>
            </div>
            <div class="input-field col l2 m2 s2" style="display:none;" id="anios">
                <label for="ano">AÑO (Ej.18)</label>
                <input type="text" name="ano" id="ano" style="width:120px" required>
            </div>
            <div class="input-field col l2 m6 s12" style="display:none;" id="boton_buscar">
                <button type="submit" name="name" value="" class="btn black" id="btn_search"><i class="material-icons">search</i>Buscar</button>
            </div>
        </form>




</div>
</div>
</div>
</div>

<!-- Esto llama a los scripts -->
<?php include '../extend/scripts.php'; ?>
<script src="../js/validacion.js"></script>
<script>
    $('#enable-option').click(function() {
        if (!$(this).is(':checked')) {
            jQuery("#seg1,#seg2,#seg3,#seg4,#periodos,#anios,#boton_buscar").css("display","none");
            jQuery("#seg1,#seg2,#seg3,#seg4").val("");
        }else {
            jQuery("#seg1").css("display","block");
        }
    });
    /// Primer campo de segmento 1 -------------------------------------
    $("#seg1").keyup(function () {
        var a = $(this).val();
        //$("#muestra_datos1").html(a);
        if (a == ""){
            $("#muestra_datos1").html("");
        }else {
            $.ajax({
                url:"controler.php",
                data:{"busqueda":a},
                type: "post",
                success:function(b){
                    $("#muestra_datos1").html(b);
                }
            });
        }
    });
    /// Segundo campo segmento 2 -------------------------------------
    $("#seg2").keyup(function () {
        var a = $(this).val();
        //$("#muestra_datos1").html(a);
        if (a == ""){
            $("#muestra_datos2").html("");
        }else {
            $.ajax({
                url:"controler.php",
                data:{"busqueda2":a},
                type: "post",
                success:function(b){
                    $("#muestra_datos2").html(b);
                }
            });
        }
    });
    /// Segundo campo segmento 3 -------------------------------------
    $("#seg3").keyup(function () {
        var a = $(this).val();
        //$("#muestra_datos1").html(a);
        if (a == ""){
            $("#muestra_datos3").html("");
        }else {
            $.ajax({
                url:"controler.php",
                data:{"busqueda3":a},
                type: "post",
                success:function(b){
                    $("#muestra_datos3").html(b);
                }
            });
        }
    });
    /// Segundo campo segmento 4 -------------------------------------
    $("#seg4").keyup(function () {
        var a = $(this).val();
        //$("#muestra_datos1").html(a);
        if (a == ""){
            $("#muestra_datos4").html("");
        }else {
            $.ajax({
                url:"controler.php",
                data:{"busqueda4":a},
                type: "post",
                success:function(b){
                    $("#muestra_datos4").html(b);
                }
            });
        }
    });
</script>
</body>
</html>


