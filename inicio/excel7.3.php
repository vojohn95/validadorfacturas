<?php
include '../Conexion/conexion.php';
require 'lib_composer/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
$hoy = date("d-m-Y");
$bday = $_POST["bdaye"];
$Date = date("d/m/Y", strtotime($bday));
$bday2 = $_POST['bday2e'];
$Date1 = date("d/m/Y", strtotime($bday2));
// Create new Spreadsheet object

$spreadsheet = new Spreadsheet();
// Set workbook properties
$spreadsheet->getProperties()->setCreator('SVF Administrador')
    ->setLastModifiedBy('Central')
    ->setTitle('Reporte')
    ->setSubject('')
    ->setDescription('Reporte.')
    ->setKeywords('Microsoft office 2016 php PhpSpreadsheet')
    ->setCategory('report');

//Set active sheet index to the first sheet,
//and add some data
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Ticket')
    ->setCellValue('B1', 'Ultimo en modificar')
    ->setCellValue('C1', 'Proveedor')
    ->setCellValue('D1', 'Rfc')
    ->setCellValue('E1', 'Fecha factura')
    ->setCellValue('F1', 'Importe IVA')
    ->setCellValue('G1', 'No.Factura')
    ->setCellValue('H1', 'UUID.')
    ->setCellValue('I1', 'Fecha carga')
    ->setCellValue('J1', 'Fecha final')
    ->setCellValue('K1', 'Estatus')
    ->setCellValue('L1', 'Comentario')
    ->setCellValue('M1', 'Prerecibo')
    ->setCellValue('N1', 'Contrarecibo');

$cell_st = [
    'font' =>['bold' => true],
    'alignment' => ['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER],
    'borders' => ['bottom' => ['style' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM]],
];
//Empieza consulta
$sel = "SELECT DISTINCT t.ticket, t.proveedor, t.rfc, t.fecha, t.importe_iva, t.num_factura,
  t.uuid, t.estatus, t.comentario , t.fecha_carga, t.fecha_final,
  IFNULL( t.pre_ticket,  'sin generar' ) AS  'pre_ticket',
  IFNULL( t.com_ticket,  'sin generar' ) AS  'com_ticket',
  u.nombre AS  'Ultimo en modificar'
  FROM ticket t, usuarios u, log g
  WHERE t.ticket = g.ticket
  AND g.id = u.id
  AND t.fecha_carga >= '".$Date."' AND t.fecha_final <= '".$Date1."'
  ORDER BY t.ticket ASC";

$consulta = mysqli_query($mysqli, $sel) or die('Error al buscar en la base de datos.');
$row = mysqli_num_rows($consulta);

$i = 2;//Numero de fila donde se va a comenzar a rellenar
while($fila=mysqli_fetch_array($consulta)) {
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.$i, $fila['ticket'])
        ->setCellValue('B'.$i, $fila['Ultimo en modificar'])
        ->setCellValue('C'.$i, $fila['proveedor'])
        ->setCellValue('D'.$i, $fila['rfc'])
        ->setCellValue('E'.$i, $fila['fecha'])
        ->setCellValue('F'.$i, $fila['importe_iva'])
        ->setCellValue('G'.$i, $fila['num_factura'])
        ->setCellValue('H'.$i, $fila['uuid'])
        ->setCellValue('I'.$i, $fila['fecha_carga'])
        ->setCellValue('J'.$i, $fila['fecha_final'])
        ->setCellValue('K'.$i, $fila['estatus'])
        ->setCellValue('L'.$i, $fila['comentario'])
        ->setCellValue('M'.$i, $fila['pre_ticket'])
        ->setCellValue('N'.$i, $fila['com_ticket']);
        $i++;
        }//Termina consulta
$spreadsheet->getActiveSheet()->getStyle('A1:M1')->applyFromArray($cell_st);
$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(17);
$spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(17);
$spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(17);
$spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(17);
$spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(17);
$spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(17);
$spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(17);
$spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(17);
$spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(17);
$spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(17);
$spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(17);
$spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(17);
$spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(17);
$spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(17);
// Set worksheet title
$spreadsheet->getActiveSheet()->setTitle('Reporte');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client's web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Reporte"'.$hoy.'".xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0


//old PhpExcel code:
//$writer = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel2007');
//$writer->save('php://output');

//new code:
$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;
