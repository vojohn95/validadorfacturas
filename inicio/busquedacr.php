<?php include '../extend/header.php';

include '../Conexion/conexion.php';

?>
 <!--Buscador en la tabla-->
 <div class="row">
   <div class="col s12" style ="width: 1500px;">
     <nav class="green lighten-1" >
       <div class="nav-wrapper" >
         <div class="input-field">
           <input type="search" id="buscar" autocomplete="off">
           <label for="buscar"><i class="material-icons">search</i></label>
           <i class="material-icons">close</i>
         </div>
       </div>
     </nav>
   </div>
 </div>
 <!-- termina buscador-->

<?php
$bday = $_POST["contrarecibo"];
//print_r($bday2);
//version 5.4
//$registros = mysql_query("SELECT * FROM ticket WHERE com_ticket = '".$bday."'") or die ("Error en consulta ".mysql_error);
//version 7.3
$query= "SELECT * FROM ticket WHERE com_ticket = '".$bday."'";
$registros = mysqli_query($mysqli, $query) or die('Error al buscar en la base de datos.');

/*while ($registro = mysql_fetch_array($registros)) {
  echo $registro['proveedor']." ".$registro['num_factura']." ".$registro['estatus'];
}*/
$row = mysqli_num_rows($registros); // con este despliego la cantidad de registros
if ($row == 0){ ?>
    <script>
        alert("Sin registros en la base de datos");
        location.href ="historial";
    </script>

    <?php
}
 ?>

 <div class="row">
   <div class="col s12" style ="width: 1330px;">
     <div class="card hoverable">
       <div class="card-content">
         <span class="card-title">Resultados de busqueda contrarecibo:(<?php echo $row ?>)</span>
         <table>
           <thead>
             <tr class="cabecera">
               <th>Ticket</th>
               <th>Proveedor</th>
               <th>RFC</th>
               <th>Fecha</th>
               <th>Importe_IVA</th>
               <th>No.Fact</th>
               <th>UUID</th>
               <th>Expedición hace</th>
               <th>Comentario</th>
              <!-- <th>Fecha carga</th>-->
               <th>Estatus</th>
               <!--
               <th>Prerecibo</th>
               <th>Contrarecibo</th>-->

             </tr>

           </thead>


<?php  while ($registro = mysqli_fetch_array($registros)) { ?>

  <tr>
    <?php

    //date_timezone_set($fol, timezone_open('America/Mexico_City'));
    //$fol->format('Y/m/d');?>
    <td><?php echo $registro['ticket'] ?></td>
    <td><?php echo $registro['proveedor'] ?></td>
    <td><?php echo $registro['rfc'] ?></td>
    <td><?php echo $registro['fecha'] ?></td>
    <td><?php echo "$". number_format($registro['importe_iva'], 2); ?></td>
    <td><?php echo $registro['num_factura'] ?></td>
    <td><?php echo $registro['uuid'] ?></td>
    <td><?php
    $date = date('m/d/Y');
    $fol = new DateTime($date);
    //echo "fol";
    //var_dump($fol);
    $reg =  $registro['fecha_cr'];
    //echo $reg;
    $regi = new DateTime($reg);
    date_timezone_set($regi, timezone_open('America/Mexico_City'));
    //$regi->format('Y/m/d');
    //echo "regi";
    //var_dump($regi);
    //var_dump($fol);
    $interval = $regi->diff($fol);
    echo $interval->format('%a días');

    ?>
    </td>
    <td><?php echo $registro['comentario'] ?></td>
    <!--<td><?php //echo $registro['fecha'] ?></td>-->
    <td><?php echo $registro['estatus'] ?></td>
    <!--
    <td><?php //echo $registro['pre_ticket'] ?></td>
    <td><?php //echo $registro['com_ticket'] ?></td>
  -->

  </tr>

<?php } ?>


</table>
</div>
</div>
</div>
<?php
 //Divide
//$registroslog = mysql_query("SELECT * FROM log WHERE pre_ticket = '".$bday."'") or die ("Error en consulta ".mysql_error);
//php 5.4
//$registroslog = mysql_query("SELECT ticket FROM log where com_ticket = '".$bday."'") or die ("Error en consulta ".mysql_error);
//version 7.3
$query= "SELECT ticket FROM log where com_ticket = '".$bday."'";
$registrolog = mysqli_query($mysqli, $query) or die('Error al buscar en la base de datos.');
/*while ($registro = mysql_fetch_array($registros)) {
  echo $registro['proveedor']." ".$registro['num_factura']." ".$registro['estatus'];
}*/
$rowlog = mysqli_num_rows($registrolog); // con este despliego la cantidad de registros
while ($registroslog = mysqli_fetch_array($registrolog)) {
    $logticket = $registroslog['ticket'];
    //echo $logticket;
}
?>

<?php
//version 5.4
 //$regticket = mysqli_query("SELECT * FROM log where ticket = '".$logticket."'") or die ("Error en consulta ".mysql_error);
 //$rowlog1 = mysqli_num_rows($regticket);
//7.3
$query= "SELECT * FROM log where ticket = '".$logticket."'";
$regticket = mysqli_query($mysqli, $query) or die('Error al buscar en la base de datos.');
$row = mysqli_num_rows($regticket);
//

 ?>

 <div class="row">
   <div class="col s12" style ="width: 1330px;">
     <div class="card hoverable">
       <div class="card-content">
         <span class="card-title">Resultados de busqueda movimientos:(<?php echo $row?>)</span>
         <table>
           <thead>
             <tr class="cabecera">
               <th>Ticket</th>
               <th>Id modifico</th>
               <th>Movimientos registrados</th>
               <th>Comentario de usuario</th>
               <th>Fecha y hora de movimiento</th>
               <th>Estatus</th>
               <!--
               <th>Prerecibo</th>
               <th>Contrarecibo</th>-->
             </tr>
           </thead>


<?php  while ($registrologticket = mysqli_fetch_array($regticket)) {  ?>

  <tr>
    <td><?php echo $registrologticket['ticket'] ?></td>

    <td><?php
     $reid = $registrologticket['id'];
     //version 5.4
     //$reticket1 = mysql_query("SELECT nombre FROM usuarios where id = '".$reid."'") or die ("Error en consulta ".mysql_error);
     //version 7.3
        $query= "SELECT nombre FROM usuarios where id = '".$reid."'";
        $reticket1 = mysqli_query($mysqli, $query) or die('Error al buscar en la base de datos.');

        while ($registrologticket1 = mysqli_fetch_array($reticket1)) {
        echo $registrologticket1['nombre'];
     }
     ?></td>
    <td><?php echo $registrologticket['comentario_sis'] ?></td>
    <td><?php echo $registrologticket['comentario_us'] ?></td>
    <td><?php echo $registrologticket['fecha_log'] ?></td>
    <td><?php echo $registrologticket['estatus_log'] ?></td>

  </tr>

<?php } ?>


</table>
</div>
<div>
  <a href="historial"><i class="material-icons">keyboard_return</i>REGRESO</a>
</div>
</div>
</div>

 <?php include '../extend/scripts.php'; ?>
 <script src="../js/validacion.js"></script>

 </html>
