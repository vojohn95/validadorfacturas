<?php
  include '../Conexion/conexion.php';
  include '../conexionEBS/conexion.php';
/*
$FIdate = $_POST['FIdate'];
$FINdate = $_POST['FINdate'];

echo $FIdate;
echo $FINdate;
*/
$stid = oci_parse($conn, 'SELECT hc.ACCOUNT_NUMBER                  "NUM CLIENTE",
       ACR.CASH_RECEIPT_ID            "ID DEL PAGO",
       RA.TRX_NUMBER                      "FACTURA",
       batc.ATTRIBUTE1                    "SERIE FACTURA",
       (batc.ATTRIBUTE1 || RA.TRX_NUMBER || '-' || ACR.CASH_RECEIPT_ID ) "FOLIO_UNICO",
       ent.name                           "ENTIDAD_ LEGAL",
       reg.REGISTRATION_NUMBER            "RFC EMISOR",
       org.NAME                           "UNIDAD OPERATIVA",
       arm.NAME                           "METODO RECIBIDO",
       ACR.GLOBAL_ATTRIBUTE2              "NUEVO SALDO DEBIDO",
       ACR.RECEIPT_NUMBER                 "NUMERO OPERACION",
       CBBV.BANK_NAME                     "NOMBRE DEL BANCO",
       CEA.BANK_ACCOUNT_NUM               "CUENTA BENEFICIARIA",
       ACR.CUSTOMER_RECEIPT_REFERENCE     "CUENTA ORDENANTE",
       CBBV.BANK_BRANCH_NAME                 "SUCURSAL",
       PAY.amount_due_original               "TOTAL DE LA FACTURA",
       NVL (ACR.amount, 0)                   "IMPORTE RECIBIDO",
       PAY.ACCTD_AMOUNT_DUE_REMAINING        "SALDO DEBIDO",
       hp.JGZZ_FISCAL_CODE                   "RECEPTOR RFC",
       ICR.CUSTOMER_NAME                     "CLIENTE",
       ICR.STATE                             "ESTADO",
       PAY.AMOUNT_APPLIED                    "MONTO APLICADO",
       DECODE (ACR.TYPE,
                 'CASH', 'STANDAR',
                 'MISC', 'VARIOS',
                 ACR.TYPE
                )                           "TIPO RECIBO",
       TO_CHAR(ARA.CREATION_DATE,'YYYY-MM-DD HH:MI:SS')                    "FECHA CONTABLE",
       TO_CHAR(ARA.APPLY_DATE,'YYYY-MM-DD HH:MI:SS')                       "FECHA RECIBO",
       ARA.STATUS                           "ESTATUS",
       ra.ATTRIBUTE12                       "TIMBRADO UUID",
       ACR.CURRENCY_CODE                    "MONEDAP",
       NVL(ACR.COMMENTS, '')                "COMENTARIOS"
FROM AR_RECEIVABLE_APPLICATIONS_ALL ARA,
     AR_CASH_RECEIPTS_ALL           ACR,
     IEX_CASH_RECEIPTS_V            ICR,
     RA_CUSTOMER_TRX_ALL            RA,
     hz_cust_accounts               hc,
     hz_parties                     hp,
     hr_all_organization_units_tl   org,
     ar_payment_schedules_all       PAY,
     xle_entity_profiles            ent,
     ar_receipt_methods             arm,
     CE_BANK_ACCOUNTS               CEA,
     CE_BANK_BRANCHES_V             CBBV,
     AR_RECEIPT_METHOD_ACCOUNTS_ALL ARMAA,
     xle_registrations              reg,
     RA_BATCH_SOURCES_all           batc,
     CE_BANK_ACCT_USES_ALL          cbaua
WHERE ARA.STATUS='APP'
AND ARA.CASH_RECEIPT_ID = ACR.CASH_RECEIPT_ID (+)
AND ARA.APPLIED_CUSTOMER_TRX_ID = RA.CUSTOMER_TRX_ID
and ent.LEGAL_ENTITY_ID  = RA.LEGAL_ENTITY_ID
AND ACR.CASH_RECEIPT_ID = ICR.CASH_RECEIPT_ID
AND ARA.org_id  = org.organization_id
AND ACR.pay_from_customer = hc.cust_account_id(+)
AND hc.party_id = hp.party_id(+)
AND PAY.CUSTOMER_ID = HC.CUST_ACCOUNT_ID
and PAY.customer_trx_id = RA.customer_trx_id
AND ACR.receipt_method_id = arm.receipt_method_id
AND ARM.RECEIPT_METHOD_ID = ARMAA.RECEIPT_METHOD_ID
AND ARMAA.REMIT_BANK_ACCT_USE_ID = cbaua.BANK_ACCT_USE_ID
and cbaua.BANK_ACCOUNT_ID = CEA.bank_account_id
AND CEA.BANK_BRANCH_ID = CBBV.BRANCH_PARTY_ID
AND batc.BATCH_SOURCE_ID = ra.BATCH_SOURCE_ID
AND ent.LEGAL_ENTITY_ID  = reg.source_id
and LANGUAGE = 'ESA'
  AND TO_CHAR(ARA.APPLY_DATE,'YYYY/MM/DD') BETWEEN '2018/08/05' AND '2018/08/07'
--AND RA.TRX_NUMBER  = '8921'
--AND hc.ACCOUNT_NUMBER  = '10054'    
--AND reg.REGISTRATION_NUMBER  = 'OCE9412073L3'
and ARA.ORG_ID = '101'
ORDER BY  ACR.RECEIPT_NUMBER DESC');
       oci_execute($stid);

echo "<table border='1'>\n";
while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
echo "<tr>\n";
foreach ($row as $item) {
   echo "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "") . "</td>\n";
}
echo "</tr>\n";
}
echo "</table>\n";
?>
