<?php include '../extend/header.php';
include '../conexionEBS/conexion.php';
?>
<div class="row">
    <div class="col s12" style ="width: 1500px;">
        <nav class="green lighten-1" >
            <div class="nav-wrapper" >
                <div class="input-field">
                    <input type="search" id="buscar" autocomplete="off">
                    <label for="buscar"><i class="material-icons">search</i></label>
                    <i class="material-icons">close</i>
                </div>
            </div>
        </nav>
    </div>
</div>
<div>
    <a href="proveedorcomp"><i class="material-icons">keyboard_return</i>REGRESO</a>
</div>
<?php
/*
$cuenta = $_POST['contrarecibo'];
echo $cuenta;
echo "<br>";
$mes = $_POST['mes'];
echo $mes;
echo "<br>";
$ano = $_POST['ano'];
echo $ano;
echo "<br>";
$completa = $mes."-".$ano;
echo $completa;
echo "<br>";echo "<br>";
$porciones = explode(".", $cuenta);
echo $porciones[0]; // porción1
echo "  ";
echo ".".$porciones[1]; // porción2
echo "  ";
echo ".".$porciones[2]; // porción2
echo "  ";
echo ".".$porciones[3]; // porción2
echo "  ";
echo ".".$porciones[4]; // porción2
echo "  ";
echo ".".$porciones[5]; // porción2
echo "  ";
echo ".".$porciones[6];
echo "  ";
echo ".".$porciones[7];
echo "<br>";*/

$seg1 = $_POST['seg1'];
$seg2 = $_POST['seg2'];
$seg3 = $_POST['seg3'];
$seg4 = $_POST['seg4'];
$mes = $_POST['mes'];
$ano = $_POST['ano'];
$fecha = $mes."-".$ano;

$stid = oci_parse($conn, "SELECT   CC.SEGMENT1||'.'||CC.SEGMENT2||'.'||CC.SEGMENT3||'.'||CC.SEGMENT4||'.'||CC.SEGMENT5||'.'||CC.SEGMENT6||'.'||CC.SEGMENT7||'.'||CC.SEGMENT8  NUMERO_CUENTA,

         SUM (NVL (gb.begin_balance_dr, 0) - NVL (gb.begin_balance_cr, 0)

             ) \"OPEN BAL\",

         NVL (gb.period_net_dr, 0) \"DEBIT\", NVL (gb.period_net_cr,

                                                 0) \"CREDIT\",

         SUM (NVL (gb.period_net_dr, 0) - NVL (gb.period_net_cr, 0)

             ) \"NET MOVEMENT\",

           SUM ((NVL (gb.period_net_dr, 0) + NVL (gb.begin_balance_dr, 0))

               )

         - SUM (NVL (gb.period_net_cr, 0) + NVL (gb.begin_balance_cr, 0))

                                                                  \"CLOSE BAL\",

           GB.LAST_UPDATE_DATE,

           gb.period_name

    FROM gl_balances gb,

         gl_code_combinations cc,

         gl_ledgers gld

   WHERE cc.code_combination_id = gb.code_combination_id

     AND gb.actual_flag = 'A'

     AND gb.template_id IS NULL

     AND gb.ledger_id = gld.ledger_id

     AND gb.period_name = '$fecha'

      AND cc.segment1 = '$seg1'   --COMPAÑIA

      AND cc.segment2 = '$seg2'   -- REGION

      AND cc.segment3 = '$seg3'  -- UNIDAD DE NEGOCIO

      AND  cc.segment4 = '$seg4'   -- CUENTA

      --AND cc.segment5 = '01001'   -- SUBCUETA

      --AND cc.segment6   -- ITERCOMPAÑIA

      --AND cc.segment7    --FUTURO

      --AND cc.segment8   -- PROYECTO



      --AND TO_CHAR(GB.LAST_UPDATE_DATE ,'YYYY/MM/DD') BETWEEN '2018/09/11' AND '2018/09/12'

GROUP BY cc.segment1,

         CC.SEGMENT2,

         cc.segment3,

         cc.segment4,

         cc.segment5,

         cc.segment6,

         CC.SEGMENT7,

         CC.SEGMENT8,

         NVL (gb.period_net_dr, 0),

         NVL (gb.period_net_cr, 0),

         gb.translated_flag,

         gb.template_id,

         GB.LAST_UPDATE_DATE,

         gb.period_name

  HAVING   SUM ((NVL (gb.period_net_dr, 0) + NVL (gb.begin_balance_dr, 0)))

         - SUM (NVL (gb.period_net_cr, 0) + NVL (gb.begin_balance_cr, 0)) <> 0

     order by CC.SEGMENT4");
oci_execute($stid);
while (oci_fetch($stid)) {
    $numCue = oci_result($stid, 'NUMERO_CUENTA');
    $openbal = oci_result($stid, 'OPEN BAL');
    $deb = oci_result($stid, 'DEBIT');
    $cred = oci_result($stid, 'CREDIT');
    $net = oci_result($stid, 'NET MOVEMENT');
    $clos = oci_result($stid, 'CLOSE BAL');
    $lud = oci_result($stid, 'LAST_UPDATE_DATE');
    $pd = oci_result($stid, 'PERIOD_NAME');

    ?>
    <div>
    <div class="row">
        <div class="col s2 m2 l2">
            <div class="card blue-white darken-1">
                <div class="card-content black-text">
                    <span class="card-title">Resultados</span>
                    <table table border='1' class="centered">
                        <thead>
                        <tr class="cabecera">
                            <th>Numero cuenta</th>
                            <th>Open bal</th>
                            <th>Debit</th>
                            <th>Credit</th>
                            <th>Net movement </th>
                            <th>Close bal</th>
                            <th>Last update date</th>
                            <th>Period name</th>

                        </tr>
                        </thead>
                        <tr>
                            <td> <?php echo $numCue; ?></td>
                            <td> <?php echo $openbal; ?></td>
                            <td> <?php echo $deb; ?></td>
                            <td> <?php echo $cred; ?></td>
                            <td> <?php echo $net; ?></td>
                            <td> <?php echo $clos; ?></td>
                            <td> <?php echo $lud; ?></td>
                            <td> <?php echo $pd; ?></td>

                        </tr>
                    </table>
                </div>

            </div>
        </div>
    </div>
    </div>
    <?php
}
$row = oci_num_rows($stid);

oci_free_statement($stid);
oci_close($conn);
if (isset($numCue) == false){ ?>
    <script>
        alert("Sin registros en ebs");
        location.href ="proveedorcomp";
    </script>

    <?php
}

for ($i = 1; $i <= $row; $i++){ ?>


<?php
}

?>
<?php include '../extend/scripts.php'; ?>

    </body>
