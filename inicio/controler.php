<?php
include '../conexionEBS/conexionProd.php';

////////////////////////////////////// PRIMERA CONSULTA DE EL SEGMENTO 1 ///////////////////////////////////////////////////
if (isset($_POST['busqueda'])){
    $strVar = $_POST['busqueda'];
    $consulta = "SELECT DISTINCT (CC.SEGMENT1) as company FROM GL_CODE_COMBINATIONS CC, GL_BALANCES BAL WHERE 1=1 AND CC.TEMPLATE_ID IS NULL AND CC.CODE_COMBINATION_ID = BAL.CODE_COMBINATION_ID AND BAL.ACTUAL_FLAG = 'A' AND ROWNUM <= 8 AND BAL.CURRENCY_CODE = 'MXN' AND BAL.LEDGER_ID = '2021' AND CC.SEGMENT1 LIKE '%".$strVar."%'";


    $strQuery = oci_parse($conn,$consulta);
    oci_execute($strQuery);
    $resulQuery = ""; $resulQuery2 = "";
    while (oci_fetch($strQuery)) {

        $resulQuery = oci_result($strQuery, 'COMPANY');
        $resulQuery2 .= "<a href='#' id='".$resulQuery."' data-value='".$resulQuery."'>" . $resulQuery . " elegir</a><br>";
    }
    echo $resulQuery2;
    echo '<script>
                $("#muestra_datos1 a").click(function () {
                    var b = $(this).attr("data-value");
                    $("#seg1").val(b);
                    $("#muestra_datos1").html("");
                    $("#seg2").css("display","block");
                });
            </script>';
}
///////////////////////////////////// SEGUNDA CONSULTA DEL SEGMENTO 2 /////////////////////////////////////////////////////////
if (isset($_POST['busqueda2'])){
    $strVar = $_POST['busqueda2'];
    $consulta = "SELECT DISTINCT (CC.SEGMENT2) as company FROM GL_CODE_COMBINATIONS CC, GL_BALANCES BAL WHERE 1=1 AND CC.TEMPLATE_ID IS NULL AND CC.CODE_COMBINATION_ID = BAL.CODE_COMBINATION_ID AND BAL.ACTUAL_FLAG = 'A' AND ROWNUM <= 8 AND BAL.CURRENCY_CODE = 'MXN' AND BAL.LEDGER_ID = '2021' AND CC.SEGMENT2 LIKE '%".$strVar."%'";


    $strQuery = oci_parse($conn,$consulta);
    oci_execute($strQuery);
    $resulQuery = ""; $resulQuery2 = "";
    while (oci_fetch($strQuery)) {

        $resulQuery = oci_result($strQuery, 'COMPANY');
        $resulQuery2 .= "<a href='#' id='".$resulQuery."' data-value='".$resulQuery."'>" . $resulQuery . " elegir</a><br>";
    }
    echo $resulQuery2;
    echo '<script>
                $("#muestra_datos2 a").click(function () {
                    var b = $(this).attr("data-value");
                    $("#seg2").val(b);
                    $("#muestra_datos2").html("");
                    $("#seg3").css("display","block");
                });
            </script>';
}
///////////////////////////////////// SEGUNDA CONSULTA DEL SEGMENTO 3 /////////////////////////////////////////////////////////
if (isset($_POST['busqueda3'])){
    $strVar = $_POST['busqueda3'];
    $consulta = "SELECT DISTINCT (CC.SEGMENT3) as company FROM GL_CODE_COMBINATIONS CC, GL_BALANCES BAL WHERE 1=1 AND CC.TEMPLATE_ID IS NULL AND CC.CODE_COMBINATION_ID = BAL.CODE_COMBINATION_ID AND BAL.ACTUAL_FLAG = 'A' AND ROWNUM <= 10 AND BAL.CURRENCY_CODE = 'MXN' AND BAL.LEDGER_ID = '2021' AND CC.SEGMENT3 LIKE '%".$strVar."%'";


    $strQuery = oci_parse($conn,$consulta);
    oci_execute($strQuery);
    $resulQuery = ""; $resulQuery2 = "";
    while (oci_fetch($strQuery)) {

        $resulQuery = oci_result($strQuery, 'COMPANY');
        $resulQuery2 .= "<a href='#' id='".$resulQuery."' data-value='".$resulQuery."'>" . $resulQuery . " elegir</a><br>";
    }
    echo $resulQuery2;
    echo '<script>
                $("#muestra_datos3 a").click(function () {
                    var b = $(this).attr("data-value");
                    $("#seg3").val(b);
                    $("#muestra_datos3").html("");
                    $("#seg4").css("display","block");
                });
            </script>';
}
///////////////////////////////////// SEGUNDA CONSULTA DEL SEGMENTO 3 /////////////////////////////////////////////////////////
if (isset($_POST['busqueda4'])){
    $strVar = $_POST['busqueda4'];
    $consulta = "SELECT DISTINCT (CC.SEGMENT4) as company FROM GL_CODE_COMBINATIONS CC, GL_BALANCES BAL WHERE 1=1 AND CC.TEMPLATE_ID IS NULL AND CC.CODE_COMBINATION_ID = BAL.CODE_COMBINATION_ID AND BAL.ACTUAL_FLAG = 'A' AND ROWNUM <= 10 AND BAL.CURRENCY_CODE = 'MXN' AND BAL.LEDGER_ID = '2021' AND CC.SEGMENT4 LIKE '%".$strVar."%'";


    $strQuery = oci_parse($conn,$consulta);
    oci_execute($strQuery);
    $resulQuery = ""; $resulQuery2 = "";
    while (oci_fetch($strQuery)) {

        $resulQuery = oci_result($strQuery, 'COMPANY');
        $resulQuery2 .= "<a href='#' id='".$resulQuery."' data-value='".$resulQuery."'>" . $resulQuery . " elegir</a><br>";
    }
    echo $resulQuery2;
    echo '<script>
                $("#muestra_datos4 a").click(function () {
                    var b = $(this).attr("data-value");
                    $("#seg4").val(b);
                    $("#muestra_datos4").html("");
                    $("#periodos").css("display","block");
                    $("#anios").css("display","block");
                    $("#boton_buscar").css("display","block");
                });
            </script>';
}
?>
