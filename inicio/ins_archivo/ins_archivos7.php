<?php
include '../../Conexion/conexion.php';
//include 'LecturaXml7.php';
include '../../conexionEBS/conexion.php';
require '../lib_composer/vendor/autoload.php';

$usuario = $_SESSION['id'];
$hoy = date("c");
$estado = "Activo";
$fcarga = date("d/m/Y");
$cont = 0;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    //echo "paso primer if<br>";
    $isgood = true;
    $nombrebase = "";
    //print_r($_FILES['ruta']['tmp_name']);
    if (count($_FILES['ruta']['tmp_name']) != 2) {
        header('location: ../../extend/alerta.php?msj=Porfavor, cargue el xml con su respectivo pdf&c=home&p=le&t=error');
        //echo "funciona si son mas de dos archivos";
    }//Termina if de count
    else {
        foreach ($_FILES["ruta"]['tmp_name'] as $key => $tmp_name) {
            //Validamos que el archivo exista
            if ($_FILES["ruta"]["name"][$key]) {
                $id = htmlentities($_POST['id']);
                $extension = '';
                $filename = $_FILES["ruta"]["name"][$key]; //Obtenemos el nombre original del archivo
                $source = $_FILES["ruta"]["tmp_name"][$key]; //Obtenemos un nombre temporal del archivo
                $info = pathinfo($filename);
                //echo "id: ".$id."<br>";
                //echo "info: ";
                //print_r($info);
                //echo "<br>";
                if ($filename != '') {
                    $extension = $info['extension'];
                    if ($extension == "xml" || $extension == "XML" || $extension == "pdf" || $extension == "PDF") {
                        if (empty($nombrebase)) {
                            $nombrebase = $info['filename'];
                            //echo $nombrebase.'--1--';
                        }
                        if ($nombrebase == $info['filename']) {
                            //echo $nombrebase.' --2-- ';
                            goto a;
                        } else {
                            $isgood = false;
                            //echo $isgood.' --3-- ';
                            header('location: ../../extend/alerta.php?msj=Los nombres de los archivos son distintos&c=arc&p=le&t=error');
                            //echo "Los nombres de los archivos son distintos";
                            break;
                        }
                    } else {
                        $isgood = false;
                        //echo $isgood.' --4-- ';
                        header('location: ../../extend/alerta.php?msj=Porfavor, cargue un archivo de formato valido!&c=arc&p=le&t=error');
                        //echo "Porfavor cargue un archivo de formato valido";
                        break;
                    }
                }//Termina filename != ''
                else {//if archivo!=
                    $isgood = false;
                    //echo $isgood.' --5--';
                    break;
                }
                a:

                if ($isgood) {
                    echo "Comienza el movimiento de los archivos a la carpeta";
                    $directorio = '../archivos'; //Declaramos un  variable con la ruta donde guardaremos los archivos

                    //Validamos si la ruta de destino existe, en caso de no existir la creamos
                    if (!file_exists($directorio)) {
                        mkdir($directorio, 0777) or die("No se puede crear el directorio de extracci&oacute;n");
                    }

                    $dir = opendir($directorio); //Abrimos el directorio de destino
                    $target_path = $directorio . '/' . $filename; //Indicamos la ruta de destino, así como el nombre del archivo

                    //Movemos y validamos que el archivo se haya cargado correctamente
                    //El primer campo es el origen y el segundo el destino
                    if (move_uploaded_file($source, $target_path)) {
                        echo "Se movio";
                        //echo "<br>";

                        //echo "El archivo $filename se ha almacenado en forma exitosa.<br>";
                        //FechaXml($source,$filename,$cont,$mysqli,$nombrebase);
                        if ($extension == "xml" || $extension == "XML") {
                            $cfdiFile = '../archivos/' . $filename;
                            $content = file_get_contents($cfdiFile);
                            $xmlContents = strstr($content, '<?xml');
                            // Añade una nueva persona al fichero
                            $actual = $xmlContents;
                            // Escribe el contenido al fichero
                            file_put_contents($cfdiFile, $actual);
                            $cfdi = \CfdiUtils\Cfdi::newFromString($xmlContents);
                            $cfdi->getVersion(); // (string) 3.3
                            $cfdi->getDocument(); // clon del objeto DOMDocument
                            $cfdi->getSource(); // (string) <cfdi:Comprobante...
                            $comprobante = $cfdi->getNode(); // Nodo de trabajo del nodo cfdi:Comprobante
                            //echo "<h3>Nodo de comprobante</h3>";
                            $compFecha = $comprobante['Fecha'];
                            //echo "<br>";
                            $compSello = $comprobante['Sello'];
                            //echo "<br>";
                            $compTotal = $comprobante['Total'];
                            //echo "<br>";
                            $compSub = $comprobante['SubTotal'];
                            //echo "<br>";
                            $compCert = $comprobante['Certificado'];
                            //echo "<br>";
                            $compForPago = $comprobante['FormaPago'];
                            //echo "<br>";
                            $compNoCert = $comprobante['NoCertificado'];
                            //echo "<br>";
                            $compTComp = $comprobante['TipoDeComprobante'];
                            //echo "<br>";
                            $compLugExp = $comprobante['LugarExpedicion'];
                            //echo "<br>";
                            $compMetPag = $comprobante['MetodoPago'];
                            //echo "<br>";
                            $compMon = $comprobante['Moneda'];
                            //echo "<br>";
                            $compConPag = $comprobante['CondicionesDePago'];
                            //echo "<br>";
                            $compFoli = $comprobante['Folio'];
                            //echo "<br>";
                            $compSerie = $comprobante['Serie'];
                            //echo "<br>";
                            $compVer = $comprobante['Version'];
                            //e//cho "<br>";
                            $compTcamb = $comprobante['TipoCambio'];
                            //echo "<br>";

                            //echo "<h3>relacionado</h3>";
                            //nodo de relacionado
                            $relacionado = $cfdi->getNode();
                            $comprobante = $relacionado->searchNodes('cfdi:CfdiRelacionados');
                            foreach ($comprobante as $comprobantes) {
                                $relTR = $comprobantes['TipoRelacion'];
                                //echo "<br>";
                            }

                            //echo "<h3>relacionados</h3>";
                            //nodo de relacionado
                            $relacionados = $cfdi->getNode();
                            $comprobante = $relacionados->searchNodes('cfdi:CfdiRelacionados', 'cfdi:CfdiRelacionado');
                            foreach ($comprobante as $comprobantes) {
                                $relTR = $comprobantes['UUID'];
                                //echo "<br>";
                            }

                            //echo "<h3>Emisor</h3>";
                            //nodo de emisor
                            $emisor = $cfdi->getNode();
                            $comprobante = $emisor->searchNodes('cfdi:Emisor');
                            foreach ($comprobante as $comprobantes) {
                                $emiRfc = $comprobantes['Rfc'];
                                //echo "<br>";
                                $emiNom = $comprobantes['Nombre'];
                                //echo "<br>";
                                $emiRF = $comprobantes['RegimenFiscal'];
                                //echo "<br>";
                            }
                            //echo "<h3>Receptor</h3>";
                            //nodo de receptor
                            $receptor = $cfdi->getNode();
                            $comprobante = $receptor->searchNodes('cfdi:Receptor');
                            foreach ($comprobante as $comprobantes) {
                                $recUcfdio = $comprobantes['UsoCFDI'];
                                //echo "<br>";
                                $recNom = $comprobantes['Nombre'];
                                //echo "<br>";
                                $recRfc = $comprobantes['Rfc'];
                                //echo "<br>";
                            }
                            //  echo "<h3>Conceptos</h3>";
                            //nodo de conceptos
                            $conceptos = $cfdi->getNode();
                            $comprobante = $conceptos->searchNodes('cfdi:Conceptos', 'cfdi:Concepto');
                            foreach ($comprobante as $comprobantes) {
                                $compCant = $comprobantes['Cantidad'];
                                //echo "<br>";
                                $compUni = $comprobantes['Unidad'];
                                //echo "<br>";
                                $compDesc = $comprobantes['Descripcion'];
                                //echo "<br>";
                                $compVU = $comprobantes['ValorUnitario'];
                                //echo "<br>";
                                $compImp = $comprobantes['Importe'];
                                //echo "<br>";
                                $compCLPS = $comprobantes['ClaveProdServ'];
                                //echo "<br>";
                                $compCALAVU = $comprobantes['ClaveUnidad'];
                                //echo "<br>";
                                $compNOID = $comprobantes['NoIdentificacion'];
                                //echo "<br>";
                            }
                            //echo "<h3>Impuestos / traslados / traslado / conceptos</h3>";
                            $xml = simplexml_load_file($cfdiFile);


                            $ns = $xml->getNamespaces(true);
                            $xml->registerXPathNamespace('c', $ns['cfdi']);
                            $xml->registerXPathNamespace('t', $ns['tfd']);

                            foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Traslado') as $Traslado) {
                                $varTrasImp = $Traslado['Importe'];
                                //echo "<br>";
                                $varTrasTasaO = $Traslado['TasaOCuota'];
                                //echo "<br>";
                                $varTrasTipF = $Traslado['TipoFactor'];
                                //echo "<br>";
                                $varTrasImpu = $Traslado['Impuesto'];
                                //echo "<br>";
                                $varTrasBase = $Traslado['Base'];
                                //echo "<br>";
                            }


                            //echo "<h3>Impuestos de impuestos</h3>";
                            //nodo de impuestos
                            $impuestos = $cfdi->getNode();
                            $comprobante = $impuestos->searchNodes('cfdi:Impuestos');
                            //echo "Total impuestos";
                            //echo "<br>";
                            foreach ($comprobante as $comprobantes) {
                                $recImpT = $comprobantes['TotalImpuestosTrasladados'];
                                //echo "<br>";
                                $recImpR = $comprobantes['TotalImpuestosRetenidos'];
                                //echo "<br>";
                            }
                            //echo "impuestos trasladados";
                            //echo "<br>";
                            foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos //cfdi:Traslados //cfdi:Traslado') as $Traslado) {
                                $varTrasImp = $Traslado['Importe'];
                                //echo "<br>";
                                $varTrasTasaO = $Traslado['TasaOCuota'];
                                //echo "<br>";
                                $varTrasTipF = $Traslado['TipoFactor'];
                                //echo "<br>";
                                $varTrasImpu = $Traslado['Impuesto'];
                                //echo "<br>";
                                //echo $varTrasBase = $Traslado['Base'];
                                //echo "<br>";
                            }
                            //echo "<h3>timbre fiscal</h3>";
                            //nodo de timbre fiscal
                            $complemento = $cfdi->getNode(); // Nodo de trabajo del nodo cfdi:Complemento
                            $comprobante = $complemento->searchNodes('cfdi:Complemento', 'tfd:TimbreFiscalDigital');
                            foreach ($comprobante as $comprobantes) {
                                $RfcSello = $comprobantes['SelloCFD'];
                                $RfcUUID = $comprobantes['UUID'];
                                $RfcVer = $comprobantes['Version'];
                                $RfcSelloSat = $comprobantes['SelloSAT'];
                                $RfcFeTim = $comprobantes['FechaTimbrado'];
                                $RfcProvCertif = $comprobantes['RfcProvCertif'];
                                $RfcNoCertif = $comprobantes['NoCertificadoSAT'];
                                //echo "<br>";
                            }//termina foreach
                            echo "lectura xml";
                            if (null === $RfcUUID) {
                                echo 'No existe el timbre fiscal digital en esta factura';
                                unlink('../archivos/' . $nombrebase . '.xml');
                                unlink('../archivos/' . $nombrebase . '.pdf');
                                header('location: ../../extend/alerta.php?msj=No existe el timbre fiscal digital en esta factura&c=arc&p=le&t=error');
                            }//Termina if de validacion de uuid
                            else {
                                //echo "existe timbre";
                                //echo "<br>";
                                //echo 'UUID: ',  $RfcUUID, PHP_EOL;
                                //echo "<br>";
                                echo " Se comprobo existencia de uuid exitosamente <br>";
                                echo "Empezo comparacion de fechas";
                                $date = date('m/d/Y');
                                $fol = new DateTime($date);
                                $regi = new DateTime($RfcFeTim);
                                date_timezone_set($regi, timezone_open('America/Mexico_City'));
                                $regi->format('Y/m/d');
                                $interval = $regi->diff($fol);
                                $dias = $interval->days;
                                if ($dias <= 31) {
                                    //echo "<br>";
                                    echo "Menor a 31 dias";
                                    //echo "<br>";
                                    echo " Comienza lista negra <br>";
                                    $stid = oci_parse($conn, "SELECT count(RFC) FROM OCE_LISTA_NEGRA WHERE RFC = '" . $emiRfc . "'");
                                    oci_execute($stid);
                                    while ($row = oci_fetch_array($stid, OCI_ASSOC + OCI_RETURN_NULLS)) {
                                        foreach ($row as $item) {
                                            ($item !== null ? htmlentities($item, ENT_QUOTES) : "");
                                        }
                                        if ($item != 0) {
                                            unlink('../archivos/' . $nombrebase . '.xml');
                                            unlink('../archivos/' . $nombrebase . '.pdf');
                                            header('location: ../../extend/alerta.php?msj=RFC en lista negra&c=arc&p=le&t=error');
                                            //echo "El siguiente rfc se encuentra en lista negra y no esta permitido: " . $emiRfc;
                                            $emiRfc;
                                            //echo "<br>";
                                        } else {

                                            echo "Limpio : " . $emiRfc;
                                            $emiRfc;

                                            $stid = oci_parse($conn, 'SELECT NUM_1099 FROM AP_SUPPLIERS WHERE NUM_1099 = :RFC');
                                            oci_bind_by_name($stid, ':RFC', $emiRfc);
                                            oci_execute($stid);
                                            if (($row = oci_fetch_array($stid, OCI_ASSOC)) == false) {
                                                echo "no esta en EBS";
                                                unlink('../archivos/' . $nombrebase . '.xml');
                                                unlink('../archivos/' . $nombrebase . '.pdf');
                                                //echo $row['NUM_1099'] ."<br>\n";
                                                header('location: ../../extend/alerta.php?msj=RFC No se encuentra en ebs&c=arc&p=le&t=error');
                                            } else {
                                                echo "esta en el ebs";

                                                //echo "<br>";
                                                //echo " Comienza Validacion en webservice<br>";
                                                error_reporting(0);
                                                $proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
                                                $proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
                                                $proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
                                                $proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
                                                $client = new nusoap_client('http://pacfe.com.mx/WSInfinityValidadorCFDi33/ValidadorCFDi33.svc?wsdl', 'wsdl',
                                                    $proxyhost, $proxyport, $proxyusername, $proxypassword);
                                                $err = $client->getError();
                                                if ($err) {
                                                    //echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
                                                }

                                                $path = $cfdiFile;
                                                $xml = simplexml_load_file($path);
                                                //con esta sacastodo tal cual we :v
                                                $content = htmlentities(file_get_contents($path));
                                                //echo $content;
                                                header("Content-Type: text/plain");
                                                $content = file_get_contents($path);
                                                //echo $content;
                                                $domain = strstr($content, '<?xml');
                                                //echo $domain;
                                                $linea2 = $domain;
                                                $client->soap_defencoding = 'UTF-8';
                                                $client->decode_utf8 = true;
                                                // Doc/lit parameters get wrapped
                                                $param = array('Usuario' => 'usrOPC',
                                                    'Contraseña' => 'usrOpc2018',
                                                    'XML' => "$linea2");
                                                $result = $client->call('Validar', array('parameters' => $param), '', '', false, true);
                                                // Check for a fault
                                                if ($client->fault) {
                                                    '<h2>Fault</h2><pre>';
                                                    //print_r($result);
                                                    '</pre>';
                                                } else {
                                                    // Check for errors
                                                    $err = $client->getError();
                                                    if ($err) {
                                                        // Display the error
                                                        //  echo '<h2>Error</h2><pre>' . $err . '</pre>';
                                                    } else {
                                                        // Display the result
                                                        //echo '<h2>Result</h2><pre>';
                                                        //print_r($result);
                                                        //echo '</pre>';
                                                    }
                                                }
                                                '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
                                                '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
                                                '<h2>Debug</h2><pre>' . htmlspecialchars($client->debug_str, ENT_QUOTES) . '</pre>';
                                                foreach ($result as $key => $value) {
                                                    $mensaje = $value['mensaje'];
                                                    $noCert = $value['ncertificado'];
                                                    $noCont = $value['ncontenido'];
                                                    $noEst = $value['nestructura'];
                                                    $noSello = $value['nsello'];
                                                    $noVersion = $value['nversion'];
                                                }

                                                $valor = 1;

                                                if ($client->fault) {
                                                    unlink('../archivos/' . $nombrebase . '.xml');
                                                    unlink('../archivos/' . $nombrebase . '.pdf');
                                                    header('location: ../../extend/alerta.php?msj=Porfavor!, contacte a su proveedor de webservice!&c=arc&p=le&t=error');
                                                } elseif ($noCert != $valor || $noCont != $valor || $noEst != $valor || $noSello != $valor || $noVersion != $valor) {
                                                    //header('location: ../../extend/alerta.php?msj=Factura no valida&c=arc&p=le&t=error');
                                                    //echo "factura no valida";
                                                    unlink('../archivos/' . $nombrebase . '.xml');
                                                    unlink('../archivos/' . $nombrebase . '.pdf');
                                                    //echo "Se borro";
                                                    header('location: ../../extend/alerta.php?msj=Factura no valida&c=arc&p=le&t=error');
                                                } else {
                                                    //header('location: ../../extend/alerta.php?msj=El usuario ha registrado la factura&c=arc&p=le&t=success');
                                                    //  echo "Comienza recibir de carga previa de UUID<br>";
                                                    //Validacion de uuid en base
                                                    $consulta = "SELECT * FROM ticket WHERE uuid='" . $RfcUUID . "' ";
                                                    $resultado = mysqli_query($mysqli, $consulta) or die (mysqli_error());
                                                    if (mysqli_num_rows($resultado) > 0) {
                                                        unlink('../archivos/' . $nombrebase . '.xml');
                                                        unlink('../archivos/' . $nombrebase . '.pdf');
                                                        header('location: ../../extend/alerta.php?msj=Factura cargada previamente&c=arc&p=le&t=error');
                                                        echo "UUID cargado previamente";
                                                    } else {
                                                        //echo "UUID nuevo para cargar";
                                                        //echo "<h3>Comienza insercion a la base de datos</h3><br>";
                                                        //$registro = "";
                                                        $insert = "INSERT INTO ticket (proveedor,rfc,fecha,importe_iva,num_factura,uuid,ruta,estatus,fecha_carga) VALUES ('" . $emiNom . "','" . $emiRfc . "','" . $compFecha . "','" . $compTotal . "','" . $compFoli . "','" . $RfcUUID . "','" . $nombrebase . "','" . $estado . "','" . $fcarga . "')";
                                                        //echo $insert;
                                                        //echo "<br>";
                                                        //$mysql_insert_query = mysqli_query($mysqli, $insert) or die('Error al insertar en la base de datos 1.');
                                                        if (mysqli_query($mysqli, $insert)) {
                                                            echo "hecho";
                                                        } else {
                                                            print_r($mysqli->error_list);
                                                        }
                                                        //echo "<br>";
                                                        $sel = "SELECT MAX(ticket) FROM ticket";
                                                        $consulta = mysqli_query($mysqli, $sel);
                                                        $f = mysqli_fetch_assoc($consulta);
                                                        //echo $sel."<br>";
                                                        $insert_log = "INSERT INTO log (ticket,id,comentario_sis,fecha_log,estatus_log) VALUES
                                                    ('" . $f['MAX(ticket)'] . "','" . $usuario . "','Cargado','" . $hoy . "','" . $estado . "')";
                                                        //echo $insert_log;
                                                        $mysql_insert_log = mysqli_query($mysqli, $insert_log) or die('Error al insertar en la base de datos 2.');
                                                        //echo "<br>";
                                                        $insert_Contfact = "INSERT INTO factura (ticket,fecha,total,subtotal,formaPago,tipodecomprobante,lugarexpedicion,metodopago,moneda,condicionesdepago,folio,totalimpuestostrasladados,importe,uuid,version,fechatimbrado,sellocfd,sellosat,rfcprovcertif,nocertificadosat) VALUES
                                                    ('" . $f['MAX(ticket)'] . "','" . $compFecha . "','" . $compTotal . "','" . $compSub . "','" . $compForPago . "','" . $compTComp . "','" . $compLugExp . "','" . $compMetPag . "','" . $compMon . "','" . $compConPag . "','" . $compFoli . "','" . $recImpT . "','" . $varTrasImp . "','" . $RfcUUID . "','" . $RfcVer . "','" . $RfcFeTim . "','" . $RfcSello . "','" . $RfcSelloSat . "','" . $RfcProvCertif . "','" . $RfcNoCertif . "')";
                                                        //echo $insert_Contfact;
                                                        $mysql_insert_contfact = mysqli_query($mysqli, $insert_Contfact) or die('Error al insertar en la base de datos 3.');
                                                        //echo "<br>";
                                                        $insert_emi = "INSERT INTO emisor (ticket,regimenfiscal,nombre,rfc) VALUES ('" . $f['MAX(ticket)'] . "','" . $emiRF . "','" . $emiNom . "','" . $emiRfc . "')";
                                                        //echo $insert_emi;
                                                        //echo "<br>";
                                                        $mysql_insert_emi = mysqli_query($mysqli, $insert_emi) or die('Error al insertar en la base de datos 4.');
                                                        //echo "<br>";
                                                        $insert_rec = "INSERT INTO receptor (ticket,usocfdi,nombre,rfc) VALUES ('" . $f['MAX(ticket)'] . "','" . $recUcfdio . "','" . $recNom . "','" . $recRfc . "')";
                                                        //echo $insert_rec;
                                                        //echo "<br>";
                                                        $mysql_insert_rec = mysqli_query($mysqli, $insert_rec) or die('Error al insertar en la base de datos 5.');
                                                        //echo "<br>";
                                                        $insert_concp = "INSERT INTO concepto (ticket , importe, valorunitario , descripcion , unidad , claveunidad, cantidad , noidentificacion , claveprodserv) VALUES
                                                    ('" . $f['MAX(ticket)'] . "','" . $compImp . "','" . $compVU . "','" . $compDesc . "','" . $compUni . "','" . $compCALAVU . "','" . $compCant . "','" . $compNOID . "','" . $compCLPS . "')";
                                                        //echo $insert_concp;
                                                        //echo "<br>";
                                                        $mysql_insert_concp = mysqli_query($mysqli, $insert_concp) or die('Error al insertar en la base de datos 6.');
                                                        //echo "<br>";
                                                        $insert_tras = "INSERT INTO traslado (ticket, importe , tasaocuota , tipofactor , impuesto , base) VALUES
                                                    ('" . $f['MAX(ticket)'] . "','" . $varTrasImp . "','" . $varTrasTasaO . "','" . $varTrasTipF . "','" . $varTrasImpu . "','" . $varTrasBase . "')";
                                                        //echo $insert_tras;
                                                        //echo "<br>";
                                                        $mysql_insert_tras = mysqli_query($mysqli, $insert_tras) or die('Error al insertar en la base de datos 7.');
                                                        //echo "<br>";
                                                        //echo "Redirecciona";
                                                        if ($mysql_insert_tras) {
                                                            header('location: ../../extend/alerta.php?msj=Factura registrada correctamente! :' . $nombrebase . '&c=home&p=le&t=success');
                                                            //echo "factura registrada correctamente";
                                                        } else {
                                                            unlink('../archivos/' . $nombrebase . '.xml');
                                                            unlink('../archivos/' . $nombrebase . '.pdf');
                                                            header('location: ../../extend/alerta.php?msj=Error al registrar en la base&c=home&p=le&t=error');
//echo "Error al registrar en la base";
                                                        }//Termina if de insert


                                                    }//Termina else uuid
                                                }//Termina else de factura no valida
// }//termina consulta rfc
// }//while segunda consulta ebs
                                            } //aqui va validacion de si esta en ebs
                                        }//termina else de lista negra
                                    }//termina while de consulta
                                }//Termina validacion de dias
                                else {
//echo "no esta en el rango";
//return false;
                                    header('location: ../../extend/alerta.php?msj=Fecha de timbrado de un mes anterior o mas&c=arc&p=le&t=error');
                                }//TErmina else
                            }//Termina else

                        }//Termina if extension
                    } else {
                        header('location: ../../extend/alerta.php?msj=Fecha de timbrado de un mes anterior o mas&c=arc&p=le&t=error');
                    }
                    closedir($dir); //Cerramos el directorio de destino
                }//Termina if de isgood
            }//Termina if de obtener los nombres
        }//Termina foreach
    }//Termina else de foreach
}//Termina if de server
else {
    header('location:../../extend/alerta.php?msj=Utiliza el formulario&c=prop&p=img&t=error');
}//Termina alerta de utiliza formulario

?>
