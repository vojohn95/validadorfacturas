<?php
include '../../Conexion/conexion.php';
require '../lib_composer/vendor/autoload.php';

function FechaXml($filename){
    //error_reporting(0);
    $xml = simplexml_load_file($filename);

    $ns = $xml->getNamespaces(true);
    $xml->registerXPathNamespace('c', $ns['cfdi']);
    $xml->registerXPathNamespace('t', $ns['tfd']);

    foreach ($xml->xpath('//t:TimbreFiscalDigital') as $tfd) {
        $val_Fecha = $tfd['FechaTimbrado'];
    }

    $date = date('m/d/Y');
    $fol = new DateTime($date);
    $regi = new DateTime($val_Fecha);
    date_timezone_set($regi, timezone_open('America/Mexico_City'));
    $regi->format('Y/m/d');
    $interval = $regi->diff($fol);
    $dias = $interval->days;
    if ($dias <= 31) {
        //global$vali;
        //$vali = true;
        return true;
    }else {
        echo "no esta en el rango";
        return false;
        header('location: ../../extend/alerta.php?msj=Fecha de timbrado de hace un mes o mas&c=arc&p=le&t=error');
    }

}//Termina la funcion de FechaXml
?>
