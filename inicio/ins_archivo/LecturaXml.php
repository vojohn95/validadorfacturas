<?php
include '../../Conexion/conexion.php';
//include '../../conexionEBS/conexion.php';

function UUIDXml($ruta,$carpeta,$cont,$localhost,$nombrebase){
  error_reporting(0);
  $xml = simplexml_load_file($ruta);

  $ns = $xml->getNamespaces(true);
  $xml->registerXPathNamespace('c', $ns['cfdi']);
  $xml->registerXPathNamespace('t', $ns['tfd']);

  foreach ($xml->xpath('//t:TimbreFiscalDigital') as $tfd) {

     $val_UUID = $tfd['UUID'];
  }

 //Validacion de rfc en base
  $consulta = "SELECT * FROM ticket WHERE uuid='".$val_UUID."' ";
  $resultado=mysql_query($consulta) or die (mysql_error());
  if (mysql_num_rows($resultado)>0)
  {
  header('location: ../../extend/alerta?msj=UUID cargado previamente&c=arc&p=le&t=error');
  } else {
  global$duplicado;
  $duplicado = true;
  //print("No Existen registros");
  }
}
//TERMINA VALIDACION CON UUID

function FechaXml($ruta,$carpeta,$cont,$localhost,$nombrebase){
  error_reporting(0);
  $xml = simplexml_load_file($ruta);

  $ns = $xml->getNamespaces(true);
  $xml->registerXPathNamespace('c', $ns['cfdi']);
  $xml->registerXPathNamespace('t', $ns['tfd']);

  foreach ($xml->xpath('//t:TimbreFiscalDigital') as $tfd) {
     $val_Fecha = $tfd['FechaTimbrado'];
  }

  $date = date('m/d/Y');
  $fol = new DateTime($date);
  $regi = new DateTime($val_Fecha);
  date_timezone_set($regi, timezone_open('America/Mexico_City'));
  $regi->format('Y/m/d');
  $interval = $regi->diff($fol);
  $dias = $interval->days;
  if ($dias <= 31) {
    global$vali;
    $vali = true;
  }else {
    echo "no esta en el rango";
    header('location: ../../extend/alerta?msj=Fecha de timbrado de un mes anterior&c=arc&p=le&t=error');
  }
}




function NegraXml($ruta,$carpeta,$cont,$localhost,$nombrebase){
  error_reporting(0);
  require_once('lib/nusoap.php');
  $hoy = date("c");
  $estado = "Activo";
  $xml = simplexml_load_file($ruta);


  $ns = $xml->getNamespaces(true);
  $xml->registerXPathNamespace('c', $ns['cfdi']);
  $xml->registerXPathNamespace('t', $ns['tfd']);

  foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor') as $Emisor){
    $val_RFC = $Emisor['Rfc'];
    //echo $val_RFC;
  }

/* Validacion de rfc en base
  $consulta = "SELECT * FROM lista_negra WHERE rfc='".$val_RFC."' ";
  $resultado=mysql_query($consulta) or die (mysql_error());
  if (mysql_num_rows($resultado)>0)
  {
  header('location: ../../extend/alerta.php?msj=Factura en lista negra&c=arc&p=le&t=error');
  } else {
  global$negra;
  $negra = true;
  //print("No Existen registros");
}*/

//consulta a listaNegra en Oracle
$proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
$proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
$proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
$client = new nusoap_client('http://localhost/pruebadfacture/WsdlEbs/webservice.php?wsdl', 'wsdl',
						$proxyhost, $proxyport, $proxyusername, $proxypassword);
$err = $client->getError();
if ($err) {
	//echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
}


$client->soap_defencoding = 'UTF-8';
$client->decode_utf8 = true;

//$result = $client->call('ConsultarValidacionesCFDI', array('parameters' => $param), '', '', false, true);
$result = $client->call('listaNegra', array('parameters' => "$val_RFC"), '', '', false, true);

// Check for a fault
if ($client->fault) {
	//echo '<h2>Fault</h2><pre>';
	//print_r($result);
	//echo '</pre>';
} else {
	// Check for errors
	$err = $client->getError();
	if ($err) {
		// Display the error
		//echo '<h2>Error</h2><pre>' . $err . '</pre>';
	} else {
		// Display the result
		//echo '<h2>Resultado</h2><pre>';
    //echo "resultado:";
		//print_r($result);
		//echo '</pre>';
	}
}
//echo'<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
//echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
//echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->debug_str, ENT_QUOTES) . '</pre>';

echo "Mensaje: ".$result;

$valor = 1;

if ($client->fault){
  header('location: ../../extend/alerta?msj=Sin conexion a webservice en central&c=arc&p=le&t=error');
}elseif($result == $valor ){
  header('location: ../../extend/alerta?msj=RFC registrado en lista negra!&c=arc&p=le&t=error');

  echo "  rfc en lista negra";
}else{
  global$negra;
  $negra = true;

  echo "no esta en lista negra";

}

}
//TERMINA VALIDACION CON LISTA NEGRA

function LeerXml($ruta,$carpeta,$cont,$localhost,$nombrebase){
//$preruta = "../archivos/";
$registro = "";
$usuario = $_SESSION['id'];

//$hoy = date("c");
$hoy = date("c");
$estado = "Activo";
$xml = simplexml_load_file($ruta);


$ns = $xml->getNamespaces(true);
$xml->registerXPathNamespace('c', $ns['cfdi']);
$xml->registerXPathNamespace('t', $ns['tfd']);

//echo 'Paso: 1';
//echo "<br />";

//EMPIEZO A LEER LA INFORMACION DEL CFDI E IMPRIMIRLA
foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante){
      //echo '----------------INFORMACION DEL COMPROBANTE----------------';
      //echo "<br />";
      //echo 'Esta es la fecha: ';
      $val_fecha= $cfdiComprobante['Fecha'];
      //echo "<br />";
      //echo 'Esta es el sello: ';
      $var_sello = $cfdiComprobante['Sello'];
      //echo "<br />";
      //echo 'Esta es el total: $';
      $varTotal = $cfdiComprobante['Total'];
      //echo "<br />";
      //echo 'Esta es el subtotal: $';
      $var_SubTComprobante = $cfdiComprobante['SubTotal'];
      //echo "<br />";
      //echo 'Esta es el certificado: ';
      $vaCertComp = $cfdiComprobante['Certificado'];
      //echo "<br />";
      //echo 'Esta es la forma de pago: ';
      $varFPComp = $cfdiComprobante['FormaPago'];
      //echo "<br />";
      //echo 'Esta es el no.certificado: ';
      $varNoCertComp = $cfdiComprobante['NoCertificado'];
      //echo "<br />";
      //echo 'Esta es el tipo de comprobante: ';
      $varTDCComp = $cfdiComprobante['TipoDeComprobante'];
      //echo "<br />";
      //echo 'Esta es el lugar de expedicion: ';
      $varLugarExpComp = $cfdiComprobante['LugarExpedicion'];
      //echo "<br />";
      //echo 'Esta es el Metodo de pago: ';
      $varMetodPagComp = $cfdiComprobante['MetodoPago'];
      //echo "<br />";
      //echo 'Esta es la moneda: ';
      $varMonedaComp = $cfdiComprobante['Moneda'];
      //echo "<br />";
      //echo 'Estas son las condiciones de pago: ';
      $varCondcPagComp = $cfdiComprobante['CondicionesDePago'];
      //echo "<br />";
      //echo 'Este es el folio: ';
      $varFol = $cfdiComprobante['Folio'];
      //echo "<br />";
      //echo "<br />";
}
//echo 'Paso: 2';
//echo "<br />";
foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos') as $Impuestos){
   //echo '----------------INFORMACION DEL IMPUESTO DE TRASLADO----------------';
   //echo "<br />";
   //echo "<br />";
   //echo 'Esta es el impuesto trasladado total: $';
   $varImpTIT = $Impuestos['TotalImpuestosTrasladados'];
   //echo "<br />";
   //echo "<br />";

}
//echo 'Paso: 3';
//echo "<br />";
foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado') as $Traslado){
   //echo '----------------INFORMACION DEL TRASLADO----------------';
   //echo "<br />";
   //echo "<br />";
   //echo 'Esta es el Importe: $';
   $varTrasladoImp = $Traslado['Importe'];
   //echo "<br />";
   //echo "<br />";

}
//echo 'Paso: 4';
//echo "<br />";
//ESTA ULTIMA FUNCIONA ESPECIFICAMENTE PARA EL TIMBRE FISCAL DIGITAL
foreach ($xml->xpath('//t:TimbreFiscalDigital') as $tfd) {
   //echo '----------------INFORMACION DEL TIMBRE FISCAL----------------';
   //echo "<br />";
   //echo "<br />";
   //echo 'Este es el sello cfd: ';
   $varTfdSeCfd = $tfd['SelloCFD'];
   //echo "<br />";
   //echo 'Este es el UUID: ';
   $val_UUID = $tfd['UUID'];
   //echo "<br />";
   //echo 'Esta es la version: ';
   $varTfdVer = $tfd['Version'];
   //echo "<br />";
   //echo 'Este es el sello del sat: ';
   $varTfdSelSAT = $tfd['SelloSAT'];
   //echo "<br />";
   //echo 'Este es la fecha de timbrado: ';
   $val_Fecha = $tfd['FechaTimbrado'];
   //echo "<br />";
   //echo 'Este es el RFC Prov.certif: ';
   $varTfdRfcProvC = $tfd['RfcProvCertif'];
   //echo "<br />";
   //echo 'Este es el no.certificado sat: ';
   $varTfdNCeSAT = $tfd['NoCertificadoSAT'];
}
//echo 'Paso: 5';
//echo "<br />";
//aqui se hace el insert a la tabla de factura
//echo 'Paso: 6';
//echo "<br />";
foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Emisor') as $Emisor){
  //echo '----------------INFORMACION DEL EMISOR----------------';
  //echo "<br />";
  //echo "<br />";
  //echo 'Este es el Regimen fiscal: ';
  $varEmiReg = $Emisor['RegimenFiscal'];
  //echo "<br />";
  //echo 'Este es el nombre del emisor: ';
  $val_Emisor = $Emisor['Nombre'];
  /*echo "<br />";
  echo 'Este es el RFC del emisor: ';*/
  $val_RFC = $Emisor['Rfc' ];/*
  echo "<br />";
  echo "<br />";*/
}

///aqui se inserta en la tabla emisor


foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Receptor') as $Receptor){
   //echo '----------------INFORMACION DEL RECEPTOR----------------';
   //echo "<br />";
   //echo "<br />";
   //echo 'Este es el UsoCFDI: ';
   $varRecepCfdi = $Receptor['UsoCFDI'];
   //echo "<br />";
   //echo 'Este es el Nombre del receptor: ';
   $varRecepNom = $Receptor['Nombre'];
   //echo "<br />";
   //echo 'Este es el rfc del receptor: ';
   $varRecepRfc = $Receptor['Rfc'];
   //echo "<br />";
   //echo "<br />";
}
$fcarga = date("d/m/Y");
//aqui se inserta en la tabla receptor


$insert = "INSERT INTO ticket (ticket,proveedor,rfc,fecha,importe_iva,num_factura,uuid,ruta,estatus,fecha_carga) VALUES ('".$registro."','".$val_Emisor."','".$val_RFC."','".$val_Fecha."','".$varTotal."','".$varFol."','".$val_UUID."','".$nombrebase."','".$estado."','".$fcarga."')";
$mysql_insert_query = mysql_query($insert, $localhost) or die (mysql_error());
//echo $insert;
$sel = "SELECT MAX(ticket) FROM ticket";
$consulta = mysql_query($sel,$localhost);
$f=mysql_fetch_assoc($consulta);
/*
'id = ".$usuario."',
fecha_log = '".$hoy."',*/

$insert_log = "INSERT INTO log (ticket,id,comentario_sis,fecha_log,estatus_log) VALUES('".$f['MAX(ticket)']."','".$usuario."','Cargado','".$hoy."','".$estado."')";
$mysql_insert = mysql_query($insert_log, $localhost) or die (mysql_error());

$insertContfact = "INSERT INTO factura (ticket,fecha,total,subtotal,formaPago,tipodecomprobante,lugarexpedicion,metodopago,moneda,condicionesdepago,folio,totalimpuestostrasladados,importe,uuid,version,fechatimbrado,sellocfd,sellosat,rfcprovcertif,nocertificadosat)
VALUES ('".$f['MAX(ticket)']."','".$val_fecha."','".$varTotal."','".$var_SubTComprobante."','".$varFPComp."','".$varTDCComp."','".$varLugarExpComp."','".$varMetodPagComp."','".$varMonedaComp."','".$varCondcPagComp."','".$varFol."','".$varImpTIT."', '".$varTrasladoImp."' , '".$val_UUID."','".$varTfdVer."','".$val_Fecha."','".$varTfdSeCfd."','".$varTfdSelSAT."','".$varTfdRfcProvC."','".$varTfdNCeSAT."')";
$mysql_insert_query_contfact = mysql_query($insertContfact, $localhost) or die (mysql_error());
//echo $insertContfact;
$insertemisor = "INSERT INTO emisor (ticket,regimenfiscal,nombre,rfc) VALUES ('".$f['MAX(ticket)']."','".$varEmiReg."','".$val_Emisor."','".$val_RFC."')";
$mysql_insert_query_contem = mysql_query($insertemisor, $localhost) or die (mysql_error());
//echo $insertemisor;
$insertrecept = "INSERT INTO receptor (ticket,usocfdi,nombre,rfc) VALUES ('".$f['MAX(ticket)']."','".$varRecepCfdi."','".$varRecepNom."','".$varRecepRfc."')";
$mysql_insert_query_conttre = mysql_query($insertrecept, $localhost) or die (mysql_error());
//echo $insertrecept;
foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto') as $Concepto){
   //echo '----------------INFORMACION DEL CONCEPTO----------------';
   //echo "<br />";
   //echo "<br />";
   //echo 'Este es el Importe: $';
   $varConImp = $Concepto['Importe'];
   //echo "<br />";
   //echo 'Este es el valor unitario: $';
   $varConValU = $Concepto['ValorUnitario'];
   //echo "<br />";
   //echo 'Este es la descripcion: ';
   $varConDes = $Concepto['Descripcion'];
   //echo "<br />";
   //echo 'Esta es la unidad: ';
   $varConUni = $Concepto['Unidad'];
   //echo "<br />";
   //echo 'Este es el Clave unidad: ';
   $varConClavU = $Concepto['ClaveUnidad'];
   //echo "<br />";
   //echo 'Este es la cantidad: ';
   $varConCant = $Concepto['Cantidad'];
   //echo "<br />";
   //echo 'Este es el No.Identificacion: ';
   $varConNoid = $Concepto['NoIdentificacion'];
   //echo "<br />";
   //echo 'Este es la clave prod.serv.: ';
   $varConClavProdS = $Concepto['ClaveProdServ'];
   //echo "<br />";
   //echo "<br />";
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   //aqui se hace el insert en la tabla CONCEPTO
   $insertconcep = "INSERT INTO concepto (ticket , importe, valorunitario , descripcion , unidad , claveunidad, cantidad , noidentificacion , claveprodserv) VALUES ('".$f['MAX(ticket)']."','".$varConImp."','".$varConValU."','".$varConDes."','".$varConUni."','".$varConClavU."','".$varConCant."','".$varConNoid."','".$varConClavProdS."')";
   $mysql_insert_query_contcon = mysql_query($insertconcep, $localhost) or die (mysql_error());
   //echo $insertconcep;
   global$variable;
   $variable = true;
}

foreach ($xml->xpath('//cfdi:Comprobante//cfdi:Conceptos//cfdi:Traslado') as $Traslado){
   //echo '----------------INFORMACION DEL TRASLADO----------------';
   //echo "<br />";
   //echo "<br />";
   //echo 'Este es el importe: $';
   $varTrasImp = $Traslado['Importe'];
   //echo "<br />";
   //echo 'Este es la Tasa0Cuota: ';
   $varTrasTasaO = $Traslado['TasaOCuota'];
   //echo "<br />";
   //echo 'Este es el tipo de factor: ';
   $varTrasTipF = $Traslado['TipoFactor'];
   //echo "<br />";
   //echo 'Este es el impuesto: ';
   $varTrasImpu = $Traslado['Impuesto'];
   //echo "<br />";
   //echo 'Este es la base: ';
   $varTrasBase = $Traslado['Base'];
   //echo "<br />";

   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      //aqui se hace el insert en la tabla traslado
      $inserttraslado = "INSERT INTO traslado (ticket, importe , tasaocuota , tipofactor , impuesto , base) VALUES ('".$f['MAX(ticket)']."','".$varTrasImp."','".$varTrasTasaO."','".$varTrasTipF."','".$varTrasImpu."','".$varTrasBase."')";
      $mysql_insert_query_tras = mysql_query($inserttraslado, $localhost) or die (mysql_error());
    //  echo $inserttraslado;
  }



//$ins = $con->prepare("INSERT INTO Ticket (Ticket,id,Proveedor,RFC,Fecha,Importe_IVA,Num_Factura,UUID,Fecha_log,Ruta,Estatus) VALUES (?,?,?,?,?,?,?,?,?,?,?);");
//$ins->bind_param(iisssssssss,$registro ,$usuario,$val_Emisor,$val_RFC,$val_Fecha,$varTotal,$varFol,$val_UUID,$hoy,$carpeta,$estado);/*'','".$nick."','".$pass1."','".$nombre."','".$correo."','".$nivel."',' 1 ','".$hoy."'*/






/*if (mysql_query($insert, $localhost)){
  return true;
}else{
  return false;
}*/

}//Cierra funtion



function ExtraerXml($ruta,$archivo,$carpeta,$cont,$localhost){
error_reporting(0);
require_once('lib/nusoap.php');

$info = pathinfo($archivo);

$nom =  $info['basename'];

$menos = (substr($nom,-3));
if($menos == "xml" || $menos == "XML"){
  //echo '<br>';
  //echo $menos;
  //echo $nom;
  //echo "hola";
  //echo '<br>';
  //if($ruta == $nom
    //$archivo = $nom;
    //print "hola";
    //echo $archivo;

    $path = $ruta;
    $xml = simplexml_load_file($path);
    //con esta sacas todo tal cual we :v
    $content = htmlentities(file_get_contents($ruta));
    //echo $content;
    header("Content-Type: text/plain");
    $content = file_get_contents($path);
    //echo $content;
    $domain = strstr($content, '<?xml');
    //echo $domain;
    $linea2= $domain;

    $proxyhost = isset($_POST['proxyhost']) ? $_POST['proxyhost'] : '';
$proxyport = isset($_POST['proxyport']) ? $_POST['proxyport'] : '';
$proxyusername = isset($_POST['proxyusername']) ? $_POST['proxyusername'] : '';
$proxypassword = isset($_POST['proxypassword']) ? $_POST['proxypassword'] : '';
$client = new nusoap_client('http://pacfe.com.mx/WSInfinityValidadorCFDi33/ValidadorCFDi33.svc?wsdl', 'wsdl',
						$proxyhost, $proxyport, $proxyusername, $proxypassword);


$err = $client->getError();
if ($err) {
	//echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
}
// Doc/lit parameters get wrapped
$param = array(
	'Usuario'    => 'usrOPC',
	'Contraseña' => 'usrOpc2018',
	'XML' => "$linea2"
);
//print_r ($param);
//print_r($param);

$client->soap_defencoding = 'UTF-8';
$client->decode_utf8 = true;

//$result = $client->call('ConsultarValidacionesCFDI', array('parameters' => $param), '', '', false, true);
$result = $client->call('Validar', array('parameters' => $param), '', '', false, true);


// Check for a fault
if ($client->fault) {
	//echo '<h2>Fault</h2><pre>';
	//print_r($result);
  //echo "fault!!";
	//echo '</pre>';
} else {
	// Check for errors
	$err = $client->getError();
	if ($err) {
		// Display the error
		//echo '<h2>Error</h2><pre>' . $err . '</pre>';
    //echo "Error!";
	} else {
		// Display the result
		//echo '<h2>Result</h2><pre>';
		//print_r($result);
    //echo "resultado!!!";
		//echo '</pre>';
	}
}
//*************************************************
foreach ($result as $key => $value) {
  $mensaje = $value['mensaje'];
  $noCert = $value['ncertificado'];
  $noCont = $value['ncontenido'];
  $noEst = $value['nestructura'];
  $noSello = $value['nsello'];
  $noVersion = $value['nversion'];
}
/*
echo $mensaje;
echo $noCert;
echo $noCont;
echo $noEst;
echo $noSello;
echo $noVersion;
*/
$valor = 1;

if ($client->fault){
  header('location: ../../extend/alerta?msj=Contacte al proveedor de webservice! &c=arc&p=le&t=error');
}elseif($noCert != $valor || $noCont != $valor || $noEst != $valor || $noSello != $valor || $noVersion != $valor){
  header('location: ../../extend/alerta?msj=Factura no valida&c=arc&p=le&t=error');
  //echo "factura no valida";
}else{
  global$variable;
  $variable = true;
  //LeerXml($ruta , $archivo,$cont,$localhost,$nombrebase);
  //header('location: ../../extend/alerta.php?msj=El usuario ha registrado la factura&c=arc&p=le&t=success');

}

}//termina comparacion para wsdl
//echo "<br>nom:";
//echo $nom;



}


?>
