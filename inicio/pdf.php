<?php
include '../Conexion/conexion.php';
setlocale(LC_ALL,"es_MX.UTF-8");
error_reporting(0);
//$id = htmlentities($_GET['id']);
$pdf = htmlentities($_GET['pdf']);
$user = $_SESSION['id'];
//Fecha_log
date_default_timezone_set('America/Mexico_City');
$date = date("d-m-y (H:i:s)");
$fechafol = date("d/m/Y");
$folio = "PR/".$fechafol."/".$pdf;
$fechaactual = getdate();
$hoy = date("c");

$up = "UPDATE ticket SET  ticket.pre_ticket= '".$folio."' WHERE ticket.ticket = ".$pdf;
$con = mysqli_query($mysqli, $up);
//echo "Hoy es: $fechaactual[weekday], $fechaactual[mday] de $fechaactual[month] de $fechaactual[year]";
$insert = "INSERT INTO log (ticket,id,comentario_sis,fecha_log,estatus_log,pre_ticket) VALUES('".$pdf."','".$user."','Se genero prerecibo','".$hoy."','Activo','".$folio."')";
$con = mysqli_query($mysqli , $insert);

$sel = "SELECT ticket,proveedor, rfc , fecha , importe_iva , num_factura , uuid, estatus, comentario FROM ticket WHERE ticket=".$pdf;
$consulta = mysqli_query($mysqli, $sel);
//  $var = mysql_fetch_assoc($consulta) or die ('no se pudo hacer la consulta'.mysql_error());
$row = mysqli_num_rows($consulta);


ob_start();
$currentsite = getcwd();
  while ($f=mysqli_fetch_assoc($consulta)) {
    $ticket = $f['ticket'];
    $sele = "SELECT moneda FROM factura WHERE ticket=".$ticket;
    $consultae = mysqli_query($mysqli, $sele);
    while ($fe=mysqli_fetch_assoc($consultae)) {
      $divisa = $fe['moneda'];
    }
 ?>

 </br>
 </br>
    <table class="striped" width="100%"  cellpadding="3" border="1">
      <tr>
        <td>
        <img src="../img/Logo.jpg" align="center" width="100px;">
      </td>
        <td align="center"><b>Datos generales de precontrarecibo</b></td>

    </tr>
    <tr>
      <td>Proveedor</td>
      <td><?php echo $f['proveedor'] ?></td>
    </tr>
    <tr>
      <td>RFC</td>
      <td><?php echo $f['rfc'] ?></td>
    </tr>
    <tr>
      <td>Importe con iva</td>
      <td><?php echo "$". number_format($f['importe_iva'], 2); ?></td>
    </tr>
    <tr>
      <td>Divisa</td>
      <td><?php echo $divisa; ?></td>
    </tr>
    <tr>
      <td>Numero de factura</td>
      <td><?php echo $f['num_factura'] ?></td>
    </tr>

    <tr>
    <td>Folio:</td>
    <td><?php echo $folio ?></td>
  </tr>

    <tr>
      <td>Fecha de expedición de este comprobante:</td>
      <td><?php echo "$fechaactual[mday] / $fechaactual[mon] / $fechaactual[year]"?></td>
    </tr>

    <tr>
      <td>Hora de expedición de este comprobante:</td>
      <td><?php echo "$fechaactual[hours] horas con $fechaactual[minutes] minutos y $fechaactual[seconds] segundos"?></td>
    </tr>


</table>
<?php
$nombre = "PR-".$f['ticket']. "-".$f['proveedor'];
}
require_once 'dompdf/autoload.inc.php';
use Dompdf\Dompdf;

$dompdf = new Dompdf();
$dompdf->loadHtml(ob_get_clean());
$dompdf->setPaper('A4' , 'portrait');
$dompdf->render();
$dompdf->stream($nombre);

?>
