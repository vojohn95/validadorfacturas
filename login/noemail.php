<?php
    //include '../Conexion/conexion.php';
		/*echo "1";
		echo $pass1;
		echo $pass2;
		echo $id;*/
 ?>
<!DOCTYPE html>
<html lang="es" >
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.css">


    <title>Gestion de Archivos</title>
  </head>
  <body>
<?php
$tituloSWAL = "Falla en el cambio";
$mensajeSWAL = "Porfavor reenvie una solicitud";
$t = 'error';
$dir = '../index';
?>

    <script
      src="https://code.jquery.com/jquery-3.1.1.min.js"
      integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
      crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.3.2/sweetalert2.js"></script>
  <script>
      swal({
        title: '<?php echo $tituloSWAL ?>',
        text: '<?php echo $mensajeSWAL ?>',
        type: '<?php echo $t ?>',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'ok'
      }).then(function(){
        location.href='<?php echo $dir ?>';
      });

      $(document).click(function(){
        location.href='<?php echo $dir ?>';
      });

      $(document).keyup(function(e){
        if (e.which == 27) {
          location.href='<?php echo $dir ?>';
        }
      });

  </script>

  </body>
</html>
