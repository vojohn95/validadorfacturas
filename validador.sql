-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 02-07-2019 a las 17:47:14
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `validador`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `cuentasContables` (IN `ticketfac` INT(11))  NO SQL
    SQL SECURITY INVOKER
    COMMENT 'si jalo :v'
DELETE FROM layouttodo WHERE ticket = ticketfac$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concepto`
--

CREATE TABLE `concepto` (
  `ticket` int(11) NOT NULL,
  `importe` varchar(50) DEFAULT NULL,
  `valorunitario` varchar(50) DEFAULT NULL,
  `descripcion` text,
  `unidad` varchar(50) DEFAULT NULL,
  `claveunidad` varchar(50) DEFAULT NULL,
  `cantidad` varchar(50) DEFAULT NULL,
  `noidentificacion` varchar(50) DEFAULT NULL,
  `claveprodserv` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `concepto`
--

INSERT INTO `concepto` (`ticket`, `importe`, `valorunitario`, `descripcion`, `unidad`, `claveunidad`, `cantidad`, `noidentificacion`, `claveprodserv`) VALUES
(1, '68.10', '68.10', 'HNR PEPPERONI', '', 'H87', '1.0', '', '90101503');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

CREATE TABLE `cuentas` (
  `id_cuenta` int(6) NOT NULL,
  `ticket` int(11) NOT NULL,
  `cuenta` varchar(50) NOT NULL,
  `monto` varchar(50) NOT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `fecha_registro` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentaslog`
--

CREATE TABLE `cuentaslog` (
  `id_cuenta` int(6) NOT NULL,
  `ticket` int(11) NOT NULL,
  `cuenta` varchar(50) NOT NULL,
  `monto` varchar(50) NOT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `fecha_registro` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emisor`
--

CREATE TABLE `emisor` (
  `ticket` int(11) NOT NULL,
  `regimenfiscal` varchar(50) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `rfc` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `emisor`
--

INSERT INTO `emisor` (`ticket`, `regimenfiscal`, `nombre`, `rfc`) VALUES
(1, '601', 'Sizzling Platter de México S.A. de C.V.', 'CCA1308213W7');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `ticket` int(11) NOT NULL,
  `fecha` varchar(50) DEFAULT NULL,
  `total` varchar(50) DEFAULT NULL,
  `subtotal` varchar(50) DEFAULT NULL,
  `formaPago` varchar(10) DEFAULT NULL,
  `tipodecomprobante` varchar(10) DEFAULT NULL,
  `lugarexpedicion` varchar(10) DEFAULT NULL,
  `metodopago` varchar(5) DEFAULT NULL,
  `moneda` varchar(5) DEFAULT NULL,
  `condicionesdepago` varchar(50) DEFAULT NULL,
  `folio` varchar(50) DEFAULT NULL,
  `totalimpuestostrasladados` varchar(50) DEFAULT NULL,
  `importe` varchar(50) DEFAULT NULL,
  `uuid` varchar(50) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `fechatimbrado` varchar(50) DEFAULT NULL,
  `sellocfd` longtext,
  `sellosat` longtext,
  `rfcprovcertif` varchar(50) DEFAULT NULL,
  `nocertificadosat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`ticket`, `fecha`, `total`, `subtotal`, `formaPago`, `tipodecomprobante`, `lugarexpedicion`, `metodopago`, `moneda`, `condicionesdepago`, `folio`, `totalimpuestostrasladados`, `importe`, `uuid`, `version`, `fechatimbrado`, `sellocfd`, `sellosat`, `rfcprovcertif`, `nocertificadosat`) VALUES
(1, '2019-02-28T15:46:48', '79.00', '68.10', '04', 'I', '06760', 'PUE', 'MXN', '', '414009', '10.90', '10.90', '4C01EBC8-9725-4640-B312-C4170C8228BF', '1.1', '2019-04-28T15:46:52', 'Ad5USlKzmp964WNK5z6wvVxLLrxzgNJeNUvOcsoukSs/y8b71fqX5dtgsqPndEYJEXjl1ed+m2CkvEw5k+Qlt8K3C8h0zOdmj/7sEtjBMSFmQuViqIDlXztgpILjPnz0NJ17BaG/pDM+duNn9eohfgw4zKDoBZugf/y94wIx0/gNWv81hPAGMzlFkWBD/ax7cibvI7jrlXWZFJRnQe5TUVfaV0VL1HBTzc495MwcWXqKleVAYh3nRQyg6m8n+/nDqZ+zE21phBjU3C8gaobDvtDl8xHXa8lQkYhpZXATjrc+IJKKnVb3q7EzAOvkMWDVqE6qA4a498IDnuBC2fdw8g==', 'bIUu4m208P7/4WJevFECco+W7ZxzVL+DWHswG59wlX2lKdKAEYBGShr1mhLUGvdIEf86SLR3G03kQAS24fGqcbiUh2yTU11IHlgpDTTOlcx6pTKBqZMI6tKP860odEKzvMtPoruwSooD6xjVxNeozNBptuHAiMU12W48ufzDQpgRapv1qY/abZQrb5zUkFAv+gesy0r7wUlnymsTiqZ2PzDlnvJ1vbElBvH8ZjjyX/I997Aj3/9YESbz8FxxM/pQnRijKbHNkSL8QH0bb5tcQCChNbhwAzV8h2CVfLdiAA8EuXbggr5SgZ3Iyx4sC1/cV5JuI7MGK/kKYzHtzrcaWQ==', 'FIN1203015JA', '00001000000405332712');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `layout`
--

CREATE TABLE `layout` (
  `ticket` varchar(11) NOT NULL,
  `supplier_num` varchar(20) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `gl_date` varchar(50) NOT NULL,
  `terms_date` varchar(50) NOT NULL,
  `terms` varchar(50) NOT NULL,
  `forma_pago` varchar(50) NOT NULL,
  `uuid` varchar(50) NOT NULL,
  `description` varchar(50) NOT NULL,
  `invoice_date` varchar(50) NOT NULL,
  `fecha_registro` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `layouttodo`
--

CREATE TABLE `layouttodo` (
  `ticket` int(11) NOT NULL,
  `contador` int(11) NOT NULL DEFAULT '0',
  `supplier_num` varchar(20) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL,
  `gl_date` varchar(50) DEFAULT NULL,
  `terms_date` varchar(50) DEFAULT NULL,
  `terms` varchar(50) DEFAULT NULL,
  `forma_pago` varchar(50) DEFAULT NULL,
  `uuid` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `invoice_date` varchar(50) DEFAULT NULL,
  `fecha_registro` varchar(50) DEFAULT NULL,
  `cuenta` varchar(50) DEFAULT NULL,
  `monto` varchar(50) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `uno` varchar(50) DEFAULT NULL,
  `dos` varchar(50) DEFAULT NULL,
  `tres` varchar(50) DEFAULT NULL,
  `fecha_final` varchar(50) DEFAULT NULL,
  `Cargado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE `log` (
  `id_log` int(5) NOT NULL,
  `ticket` int(11) NOT NULL,
  `id` int(5) NOT NULL,
  `comentario_sis` varchar(100) DEFAULT NULL,
  `comentario_us` varchar(100) DEFAULT NULL,
  `fecha_log` varchar(50) NOT NULL,
  `estatus_log` varchar(25) NOT NULL,
  `pre_ticket` varchar(50) DEFAULT NULL,
  `com_ticket` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`id_log`, `ticket`, `id`, `comentario_sis`, `comentario_us`, `fecha_log`, `estatus_log`, `pre_ticket`, `com_ticket`) VALUES
(1, 1, 3, 'Cargado', NULL, '2019-05-09T20:56:50+00:00', 'Activo', NULL, NULL),
(2, 1, 3, 'Validado por Mesa de control', NULL, '2019-05-09T20:57:28+00:00', 'MESA', NULL, NULL),
(3, 1, 5, 'Validado por compras', NULL, '2019-05-09T20:57:45+00:00', 'COMPRAS', NULL, NULL),
(4, 1, 6, 'Devuelto por Cuentas por pagar', NULL, '2019-05-09T20:58:00+00:00', 'RECHAZADO', NULL, NULL),
(5, 1, 3, 'Validado por compras', NULL, '2019-05-09T20:58:47+00:00', 'COMPRAS', NULL, NULL),
(6, 1, 6, 'Validado por Cuentas por Pagar', NULL, '2019-05-09T20:59:23+00:00', 'CUENTASPORPAGAR', NULL, NULL),
(7, 1, 8, 'Devuelto por contabilidad', NULL, '2019-05-09T20:59:48+00:00', 'RECHAZADO', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `receptor`
--

CREATE TABLE `receptor` (
  `ticket` int(11) NOT NULL,
  `usocfdi` varchar(10) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `rfc` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `receptor`
--

INSERT INTO `receptor` (`ticket`, `usocfdi`, `nombre`, `rfc`) VALUES
(1, 'G03', 'Luis Fernando Jonathan Vargas Osornio', 'VAOL9504286P9');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `ticket` int(11) NOT NULL,
  `proveedor` varchar(250) NOT NULL,
  `rfc` varchar(13) NOT NULL,
  `fecha` varchar(50) NOT NULL,
  `importe_iva` varchar(50) NOT NULL,
  `num_factura` varchar(50) NOT NULL,
  `uuid` varchar(50) NOT NULL,
  `extras` varchar(100) DEFAULT NULL,
  `extension` varchar(100) DEFAULT NULL,
  `ruta` varchar(100) DEFAULT NULL,
  `estatus` varchar(25) NOT NULL,
  `comentario` varchar(100) DEFAULT NULL,
  `pre_ticket` varchar(50) DEFAULT NULL,
  `com_ticket` varchar(50) DEFAULT NULL,
  `fecha_cr` varchar(50) DEFAULT NULL,
  `fecha_carga` varchar(50) DEFAULT NULL,
  `fecha_final` varchar(50) DEFAULT NULL,
  `comen_cuen` varchar(50) DEFAULT 'Sin comentarios'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`ticket`, `proveedor`, `rfc`, `fecha`, `importe_iva`, `num_factura`, `uuid`, `extras`, `extension`, `ruta`, `estatus`, `comentario`, `pre_ticket`, `com_ticket`, `fecha_cr`, `fecha_carga`, `fecha_final`, `comen_cuen`) VALUES
(1, 'Sizzling Platter de México S.A. de C.V.', 'CCA1308213W7', '2019-02-28T15:46:48', '79.00', '414009', '4C01EBC8-9725-4640-B312-C4170C8228BF', NULL, NULL, 'xmlinv_5c7856ccbf0617.00685634 (1)', 'CONTABILIDADD', NULL, NULL, NULL, NULL, '09/05/2019', NULL, 'Sin comentarios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traslado`
--

CREATE TABLE `traslado` (
  `ticket` int(11) NOT NULL,
  `importe` varchar(50) DEFAULT NULL,
  `tasaocuota` varchar(50) DEFAULT NULL,
  `tipofactor` varchar(50) DEFAULT NULL,
  `impuesto` varchar(50) DEFAULT NULL,
  `base` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `traslado`
--

INSERT INTO `traslado` (`ticket`, `importe`, `tasaocuota`, `tipofactor`, `impuesto`, `base`) VALUES
(1, '10.90', '0.160000', 'Tasa', '002', '68.10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(5) NOT NULL,
  `nivel` varchar(50) NOT NULL,
  `nick` varchar(30) NOT NULL,
  `pass` varchar(130) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `correo` varchar(80) NOT NULL,
  `last_session` datetime DEFAULT NULL,
  `bloqueo` int(1) NOT NULL,
  `token` varchar(50) DEFAULT NULL,
  `pass_r` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nivel`, `nick`, `pass`, `nombre`, `correo`, `last_session`, `bloqueo`, `token`, `pass_r`) VALUES
(1, 'HISTORIAL', 'JOHNADMIN', 'bab8518a923c061567a34df0d583748a', 'ADMINISTRADOR', 'adm@correo.com', NULL, 1, NULL, NULL),
(2, 'ADMINISTRADOR', 'VICTORRB', '25f9e794323b453885f5181f1b624d0b', 'VICTOR ROJAS BARRERA', 'vr@correo.com', '2018-05-21 00:00:00', 1, NULL, NULL),
(3, 'ADMINISTRADOR', 'JOHNVARGAS', '25f9e794323b453885f5181f1b624d0b', 'JOHN', 'lfernando@integrador-technology.mx', NULL, 1, '5vki2aem73n00aed4984o25dx6ctp1gb8ujhqwfcr91ls1zy8', 1),
(4, 'MESADECONTROL', 'MESADECONTROL', 'bab8518a923c061567a34df0d583748a', 'MESADECONTROL', 'mc@correo.com', '0000-00-00 00:00:00', 1, NULL, NULL),
(5, 'COMPRAS', 'COMPRASUSU', 'bab8518a923c061567a34df0d583748a', 'COMPRAS USU', 'vrojas@integrador-technology.mx', '0000-00-00 00:00:00', 1, NULL, NULL),
(6, 'CUENTASPORPAGAR', 'CUENTASPORPAGAR', 'bab8518a923c061567a34df0d583748a', 'CUENTAS POR PAGAR', 'vojohn95@gmail.com', '0000-00-00 00:00:00', 1, 'egekm0swi0ocaa6394lpjyb085r315u78q5tzdhv2en0fxd2b', 1),
(7, 'CONTABILIDAD', 'CONTABILIDAD', 'bab8518a923c061567a34df0d583748a', 'CONTABILIDAD', 'vojohn95@g', '0000-00-00 00:00:00', 1, NULL, NULL),
(8, 'CONTABILIDAD', 'SERGIOUSUARIO', 'bab8518a923c061567a34df0d583748a', 'SERGIO', 'elias@integrador-technology.mx', '0000-00-00 00:00:00', 1, NULL, NULL),
(9, 'MESADECONTROL', 'OLIVERADMIN', 'bab8518a923c061567a34df0d583748a', 'OLIVER', 'co@correo.com', NULL, 1, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `concepto`
--
ALTER TABLE `concepto`
  ADD KEY `FK_id_c` (`ticket`);

--
-- Indices de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD PRIMARY KEY (`id_cuenta`),
  ADD KEY `FK_id_cue` (`ticket`);

--
-- Indices de la tabla `cuentaslog`
--
ALTER TABLE `cuentaslog`
  ADD PRIMARY KEY (`id_cuenta`),
  ADD KEY `FK_id_clog` (`ticket`);

--
-- Indices de la tabla `emisor`
--
ALTER TABLE `emisor`
  ADD KEY `FK_id_e` (`ticket`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`uuid`),
  ADD KEY `FK_id_` (`ticket`);

--
-- Indices de la tabla `layout`
--
ALTER TABLE `layout`
  ADD KEY `FK_uuid_fac` (`uuid`);

--
-- Indices de la tabla `layouttodo`
--
ALTER TABLE `layouttodo`
  ADD KEY `FK_id_latt` (`ticket`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id_log`),
  ADD KEY `FK_id` (`id`),
  ADD KEY `FK_ticket` (`ticket`);

--
-- Indices de la tabla `receptor`
--
ALTER TABLE `receptor`
  ADD KEY `FK_id_r` (`ticket`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`ticket`),
  ADD UNIQUE KEY `uuid` (`uuid`),
  ADD UNIQUE KEY `pre_ticket` (`pre_ticket`),
  ADD UNIQUE KEY `com_ticket` (`com_ticket`);

--
-- Indices de la tabla `traslado`
--
ALTER TABLE `traslado`
  ADD KEY `FK_id_tr` (`ticket`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nick` (`nick`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cuentas`
--
ALTER TABLE `cuentas`
  MODIFY `id_cuenta` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cuentaslog`
--
ALTER TABLE `cuentaslog`
  MODIFY `id_cuenta` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `log`
--
ALTER TABLE `log`
  MODIFY `id_log` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `ticket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `concepto`
--
ALTER TABLE `concepto`
  ADD CONSTRAINT `FK_id_c` FOREIGN KEY (`ticket`) REFERENCES `ticket` (`ticket`);

--
-- Filtros para la tabla `cuentas`
--
ALTER TABLE `cuentas`
  ADD CONSTRAINT `FK_id_cue` FOREIGN KEY (`ticket`) REFERENCES `ticket` (`ticket`);

--
-- Filtros para la tabla `cuentaslog`
--
ALTER TABLE `cuentaslog`
  ADD CONSTRAINT `FK_id_clog` FOREIGN KEY (`ticket`) REFERENCES `ticket` (`ticket`);

--
-- Filtros para la tabla `emisor`
--
ALTER TABLE `emisor`
  ADD CONSTRAINT `FK_id_e` FOREIGN KEY (`ticket`) REFERENCES `ticket` (`ticket`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `FK_id_` FOREIGN KEY (`ticket`) REFERENCES `ticket` (`ticket`);

--
-- Filtros para la tabla `layout`
--
ALTER TABLE `layout`
  ADD CONSTRAINT `FK_uuid_fac` FOREIGN KEY (`uuid`) REFERENCES `factura` (`uuid`);

--
-- Filtros para la tabla `layouttodo`
--
ALTER TABLE `layouttodo`
  ADD CONSTRAINT `FK_id_latt` FOREIGN KEY (`ticket`) REFERENCES `ticket` (`ticket`);

--
-- Filtros para la tabla `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `FK_id` FOREIGN KEY (`id`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `FK_ticket` FOREIGN KEY (`ticket`) REFERENCES `ticket` (`ticket`);

--
-- Filtros para la tabla `receptor`
--
ALTER TABLE `receptor`
  ADD CONSTRAINT `FK_id_rec` FOREIGN KEY (`ticket`) REFERENCES `ticket` (`ticket`);

--
-- Filtros para la tabla `traslado`
--
ALTER TABLE `traslado`
  ADD CONSTRAINT `FK_id_tras` FOREIGN KEY (`ticket`) REFERENCES `ticket` (`ticket`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
